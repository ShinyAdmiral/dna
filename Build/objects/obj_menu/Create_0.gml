/// @description menu variables
pointer = 1;
max_options = 2;
in_menu = false;
true_pause = true;
active = false;
screen_saved = true;
spr_pause = spr_solid_16;
first = false;
mouse_previous_x = 0;
mouse_previous_y = 0;
joy_stick_up_cool = false;
joy_stick_down_cool = false;

depth = -room_height -30