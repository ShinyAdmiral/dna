/// @description Menu Functionality and visuals

if (active){
	draw_sprite_ext(spr_pause,0,camera_get_view_x(view_camera[0]),camera_get_view_y(view_camera[0]),view_get_wport(0)/display_get_width(), view_get_hport(0)/display_get_height(),0, c_white, 1);
	
	if (in_menu){
		menu_up = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(0, gp_padu);
		menu_down = keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down) || gamepad_button_check_pressed(0, gp_padd);
		menu_enter = keyboard_check_pressed(vk_enter) || keyboard_check_pressed(ord("E")) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, gp_face1) || mouse_check_button_pressed(mb_left);
		menu_escape = keyboard_check_pressed(vk_escape) || keyboard_check_pressed(ord("Q")) || keyboard_check_pressed(vk_backspace) || gamepad_button_check_pressed(0, gp_face2) || gamepad_button_check_pressed(0, gp_start);
	
		if (menu_down)
			pointer++;
		else if (menu_up)
			pointer--;
	
		gamepad_set_axis_deadzone(0,.8);
		menu_up_joy = -gamepad_axis_value(0,gp_axislv);
		menu_down_joy = gamepad_axis_value(0,gp_axislv);

		if (menu_up_joy && !joy_stick_up_cool){
			instance_destroy(obj_Menu_Cursor);
			mouse_previous_x = mouse_x;
			mouse_previous_y = mouse_y;
			pointer--;
			joy_stick_up_cool = true;
			joy_stick_down_cool = false;
			alarm[0] = room_speed / 6;
		}
		if (menu_down_joy && !joy_stick_down_cool){
			instance_destroy(obj_Menu_Cursor);
			mouse_previous_x = mouse_x;
			mouse_previous_y = mouse_y;
			pointer++;
			joy_stick_up_cool = false;
			joy_stick_down_cool = true;
			alarm[1] = room_speed / 6;
		}


		if (pointer > max_options){
			pointer = 1;
		}
		else if (pointer < 1){
			pointer = max_options;
		}
	
		if ((menu_up || menu_down || menu_enter|| menu_escape) && !first){
			instance_destroy(obj_Menu_Cursor);
			mouse_previous_x = mouse_x;
			mouse_previous_y = mouse_y;
		}
	
		if (instance_exists(obj_Menu_Cursor)){
			if (obj_Menu_Cursor.y < room_height/2){
				pointer = 1;
			}
			else if (obj_Menu_Cursor.y < room_height/2+32){
				pointer = 2;
			}
		}
		else{
			if ((mouse_previous_x != mouse_x) || (mouse_previous_y != mouse_y)){
				instance_create_depth(mouse_x, mouse_y, -3000, obj_Menu_Cursor);
			}
		}
	
		if (menu_escape && !first){
			active = false;
			escape = false;
			screen_saved = true;
			pointer = 1;
			in_menu = false;
			instance_destroy(obj_Menu_Cursor)
			instance_activate_layer("Instances");
		}
	
		switch (pointer){
		case 1:
			draw_set_color(c_blue);
			draw_text(camera_get_view_x(view_camera[0]) + (camera_get_view_width(view_camera[0])/2) - 32, camera_get_view_x(view_camera[0]) + ((camera_get_view_height(view_camera[0])/2)) -32, "Resume");
			draw_set_color(c_yellow);
			draw_text(camera_get_view_x(view_camera[0]) + (camera_get_view_width(view_camera[0])/2) - 32, camera_get_view_x(view_camera[0]) + ((camera_get_view_height(view_camera[0])/2)), "Quit");
			if (menu_enter){
				active = false;
				escape = false;
				screen_saved = true;
				pointer = 1;
				in_menu = false;
				instance_destroy(obj_Menu_Cursor)
				instance_activate_object(obj_player);
				instance_activate_layer("Instances");
			}
			break;
		
		case 2:
			draw_set_color(c_yellow);
			draw_text(camera_get_view_x(view_camera[0]) + (camera_get_view_width(view_camera[0])/2) - 32, camera_get_view_x(view_camera[0]) + ((camera_get_view_height(view_camera[0])/2)) -32, "Resume");
			draw_set_color(c_blue);
			draw_text(camera_get_view_x(view_camera[0]) + (camera_get_view_width(view_camera[0])/2) - 32, camera_get_view_x(view_camera[0]) + ((camera_get_view_height(view_camera[0])/2)), "Quit");
			if (menu_enter){
				game_end();
			}
			break;
		}
	
		first = false;
	}
}