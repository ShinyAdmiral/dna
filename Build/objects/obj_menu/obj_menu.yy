{
    "id": "2537c8bc-2ff4-494d-a0c8-6a3dadd7ef3a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Menu",
    "eventList": [
        {
            "id": "abfe3548-4a8b-4eee-9267-ee060668799f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2537c8bc-2ff4-494d-a0c8-6a3dadd7ef3a"
        },
        {
            "id": "c6786a9e-685e-4087-a85b-4a5dbc15998b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2537c8bc-2ff4-494d-a0c8-6a3dadd7ef3a"
        },
        {
            "id": "bd6e4ef6-dc87-4a83-b1f7-aa801cae63bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2537c8bc-2ff4-494d-a0c8-6a3dadd7ef3a"
        },
        {
            "id": "fe450d57-e115-4cd7-b80a-ad8675ebd106",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2537c8bc-2ff4-494d-a0c8-6a3dadd7ef3a"
        },
        {
            "id": "8a8bca24-407e-474d-b7fa-459f63dde6ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2537c8bc-2ff4-494d-a0c8-6a3dadd7ef3a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}