/// @description Activate Menu
escape = keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(0,gp_start);

if ((escape) && (!active)){
	if (screen_saved){
		screen_save("Pause.png");
		spr_pause = sprite_add("Pause.png", 1, false, false, 0, 0);
		if (!gamepad_is_connected(0)){
			instance_create_depth(mouse_x, mouse_y, -3000, obj_Menu_Cursor);
		}
		screen_saved = false;	
	}
	instance_deactivate_layer("Instances");
	instance_deactivate_object(obj_player);
	active = true;
	escape = false;
	first = true;
	in_menu = true;
}

else if ((escape) && (active)){
	active = false;
	escape = false;
	screen_saved = true;
	pointer = 1;
	in_menu = false;
	instance_destroy(obj_Menu_Cursor)
	instance_activate_object(obj_player);
	instance_activate_layer("Instances");
}