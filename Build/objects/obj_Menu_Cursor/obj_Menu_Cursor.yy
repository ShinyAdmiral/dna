{
    "id": "7f37d33f-b3f9-4329-9ea9-b1d439572877",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Menu_Cursor",
    "eventList": [
        {
            "id": "54395353-3063-41df-a95e-6a996e005e1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f37d33f-b3f9-4329-9ea9-b1d439572877"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a006e31-6bd3-40b0-bfe5-12afed0d3cd1",
    "visible": true
}