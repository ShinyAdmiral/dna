{
    "id": "09d8bcef-caf9-4144-94aa-f24ca2571c55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Teeter_Collider_Down",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "19ef5ae2-56e8-4260-9121-385c46f2501d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "z",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "abcd6ec1-fd4c-4e61-8a28-68d89f6a9ee6",
    "visible": true
}