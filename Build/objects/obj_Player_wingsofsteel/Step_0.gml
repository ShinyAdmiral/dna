/// @description State Machine
Depth_Update(0);

switch state {
	case(Test_Player_Steel_Wings.normal):
	
		#region normal State
		move_speed = original_move_speed;

		Player_Input();

		Player_Movement_Calc();
		
		#region Z Movement
		//Calc Jumping
		if (z <= zfloor){
			if (jump){
				zjump = true;
			}
		}

		if (zjump){
			inst = noone;
			inst = instance_place(x,y,obj_float_solid);
			if ((inst != noone) and (z < inst.z)){
				if (inst.z-inst.height-height > 0){
					z = clamp(z + zspeed, 0, inst.z-inst.height-height); // z pos goes up
				}
			}
			else{
				z += zspeed;
			}
		}

		//if not ontop of block
		if ((!instance_place(x,y,obj_solid)) and (!instance_place(x,y,obj_float_solid))){
			zfloor = 0; /*zfloor is ground level*/
		}

		//if not on ground
		if (!z <= zfloor){
			z -= zgrav; //apply downforce on z pos
			zgrav += zacce; //rav gets stronger each step
		}

		//stop falling when you hit zfloor
		if (z <= zfloor+1/*+1 for sticking glitch on ground*/){
			zgrav = 0; //stop applying downforce
			z = zfloor;	//nap z pos to on ground
			zjump = false; //no longer in the air
			roll_able = true;
		}
		#endregion
		
		Edge_Teeter();
		
		Player_Collisions_Check();
		
		Player_Normal_Drawing();
		
		#endregion
		
		#region switch state
		
		//check for rolling input
		if (roll_player and roll_able){
			state = Test_Player_Steel_Wings.dash;
			roll_anticipate = true;
			roll_timer = true;
			roll_able = false;
			break;
		}
		
		if (keyboard_check(roll) and !roll_able and z > zfloor){
			state = Test_Player_Steel_Wings.glide;
		}
		
		if (mouse_check_button_pressed(attack_1)){
			punch = true;
			state = Test_Player_Steel_Wings.melee;
			break;
		}
		
		if (mouse_check_button_pressed(attack_2)){
			throw = true;
			state = Test_Player_Steel_Wings.range;
			break;
		}
		
		#endregion
		
		break;
	
	///NOT DONE (NEEDS WORK ON HITBOXES AND SPRITES)
	case(Test_Player_Steel_Wings.dash):
	
		#region rolling state
			
		//set roll speed based on direction
		roll_hor_speed = dcos(dir) * roll_speed;
		roll_ver_speed = dsin(dir) * roll_speed;

		//calculate roll speed 
		RH_max = sqrt(sqr(roll_speed) - sqr(roll_ver_speed));
		RV_max = sqrt(sqr(roll_speed) - sqr(roll_hor_speed));

		//frame of anticipation. Makes the user stop before the roll
		if (roll_anticipate){
			roll_anticipate = false;
			image_speed = 0;
			
			//chnage SPRITE AND DIRECTION HERE
			if (dir > 90 and dir < 270){
				sprite_index = spr_Roll_left;
			}
			else{
				sprite_index = spr_Roll_right;
			}
	
			image_index = 0;
			alarm[0] = room_speed * roll_anticipation_lag;
		}


		if (rolling){			
			if (roll_timer){
				alarm[0] = room_speed * roll_duration;
				roll_timer = false;
				image_speed = 1;
			}
			
			// Collisions and Movement
			#region Horizontal Collision
			//right collision check
			if (roll_hor_speed > 0){
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x + abs(roll_hor_speed),y,obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1,y,insts)){
						x ++;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x + abs(roll_hor_speed),y,obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if ((instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height)){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1,y,instf))
					{
						x ++;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
			}

			//left collision check
			if (roll_hor_speed < 0){
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x - abs(roll_hor_speed),y,obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1,y,insts))
					{
						x --;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x - abs(roll_hor_speed),y,obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1,y,instf))
					{
						x --;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
			}

			//move player on the x axis after collision checks
			x += clamp(roll_hor_speed, -RH_max, RH_max);
			#endregion

			#region Vertical Collision
			//down collision check
			if (roll_ver_speed > 0){
	
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x,y - abs(roll_ver_speed),obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1,insts))
					{
						y --;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x,y - abs(roll_ver_speed),obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1,instf)){
						y --;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
			}

			// up collision check
			if (roll_ver_speed < 0){
	
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x,y + abs(roll_ver_speed),obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y + 1,insts)){
						y ++;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x,y + abs(roll_ver_speed),obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y + 1,instf)){
						y ++;
					}
		
					//stop
					roll_hor_speed = 0;
					rolle_ver_speed = 0;
					rolling = false;
					state = Test_Player_Steel_Wings.normal;
				}
			}

			//move player on the y axis after collision checks
			y += clamp(-roll_ver_speed, -RV_max, RV_max);
			#endregion
		}
		
		#endregion
		
		break;
	
	///NOT DONE (NEEDS WORK ON HITBOXES AND SPRITES)
	case(Test_Player_Steel_Wings.glide):
		
		#region gliding state
		
		if (dir < 22.5){
			sprite_index = spr_wingsofsteel_glide_E
		}
		else if (dir < 67.5){
			sprite_index = spr_wingsofsteel_glide_NE
		}
		else if (dir < 112.5){
			sprite_index = spr_wingsofsteel_glide_N
		}
		else if (dir < 157.5){
			sprite_index = spr_wingsofsteel_glide_NW
		}
		else if (dir < 202.5){
			sprite_index = spr_wingsofsteel_glide_W
		}
		else if (dir < 247.5){
			sprite_index = spr_wingsofsteel_glide_SW
		}
		else if (dir < 292.5){
			sprite_index = spr_wingsofsteel_glide_S
		}
		else if (dir < 337.5){
			sprite_index = spr_wingsofsteel_glide_SE
		}
		else{
			sprite_index = spr_wingsofsteel_glide_E
		}
		
		Player_Input();
		
		Player_Movement_Calc();
		
		#region ZCalc Z movement

		//if not ontop of block
		if ((!instance_place(x,y,obj_solid)) and (!instance_place(x,y,obj_float_solid))){
			zfloor = 0; /*zfloor is ground level*/
		}

		//if not on ground
		if (!z <= zfloor){
			z -= zgrav; //apply downforce on z pos
			grav_acce = zacce;
			zgrav += grav_acce/glide_power; //grav gets stronger each step (but it's reduced)
		}

		//stop falling when you hit zfloor
		if (z <= zfloor+1/*+1 for sticking glitch on ground*/){
			zgrav = 0; //stop applying downforce
			z = zfloor;	//nap z pos to on ground
			zjump = false; //no longer in the air
			roll_able = true;
			state = Test_Player_Steel_Wings.normal;
		}
		
		#endregion
		
		Player_Collisions_Check();
		
		#endregion
		
		#region change state
		
		//check for roll
		if (keyboard_check_released(roll)){
			state = Test_Player_Steel_Wings.normal;
		}
		
		#endregion
		
		break;
	
	case(Test_Player_Steel_Wings.melee): 
		sprite_index = spr_skeleton_attack_frame;
		
		if (punch){
			alarm[2] = 5;
			var a = instance_create_layer(x+21,y-30, "Hit_Boxes", obj_hitbox_test);
			a.z = z;
			a.dir = dir;
			punch = false;
		}
		
		break;
	
	case(Test_Player_Steel_Wings.range):
		
		if (throw){
			alarm[3] = 15;
			var a = instance_create_layer(x+21,y-30, "Instances", obj_steel_wing_player_projectile);
			a.z = z;
			a.dir = dir;
			a.speed = projectile_speed;
			
			var b = instance_create_layer(x+21,y-30, "Instances", obj_steel_wing_player_projectile);
			b.z = z;
			b.dir = dir + 20;
			b.speed = projectile_speed;
			
			var c = instance_create_layer(x+21,y-30, "Instances", obj_steel_wing_player_projectile);
			c.z = z;
			c.dir = dir - 20;
			c.speed = projectile_speed;
			throw = false;
		}
		
		break;
	
	case(Test_Player_Steel_Wings.hurt):
	
	#region hurt state function
		if (!scr_stun){
			zjump = true;
			zspeed = scr_vertical;
			
			hor_speed = lengthdir_x(scr_horizontal, scr_dir);	
			ver_speed = lengthdir_y(scr_horizontal, scr_dir) * -1;
			
			scr_stun = true;
		}
		
		if (zjump){
		
			#region movement
		
			#region Horizontal
				//right collision check
				if (hor_speed > 0){
					//Grounded Solids---------------------------------------------------------------
					//reset instance for new instance
					insts = noone;
	
					//check for instance
					insts = (instance_place(x + abs(hor_speed),y,obj_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (insts != noone) and (insts.z > z){
						//ensures smooth and consistant collision
						while(!place_meeting(x + 1,y,insts)){
							x ++;
						}
		
						//stop
						hor_speed = -hor_speed/buoyancy;
					}
	
					//Floatingh Solids---------------------------------------------------------------
					//reset instance for new instance
					instf = noone;
	
					//check for instance
					instf = (instance_place(x + abs(hor_speed),y,obj_float_solid));
	
					//if instanceexists and is taller than players z stop the player
					if ((instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height)){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x + 1,y,instf))
						{
							x ++;
						}
		
						//stop
						hor_speed = -hor_speed/buoyancy;
					}
				}

				//left collision check
				if (hor_speed < 0){
					//Grounded Solids---------------------------------------------------------------
					//reset instance for new instance
					insts = noone;
	
					//check for instance
					insts = (instance_place((x - abs(hor_speed)),y,obj_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (insts != noone) and (insts.z > z){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x - 1,y,insts))
						{
							x --;
						}
		
						//stop
						hor_speed = -hor_speed/buoyancy;
					}
	
					//Floatingh Solids---------------------------------------------------------------
					//reset instance for new instance
					instf = noone;
	
					//check for instance
					instf = (instance_place(x - abs(hor_speed),y,obj_float_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x - 1,y,instf))
						{
							x --;
						}
		
						//stop
						hor_speed = -hor_speed/buoyancy;
					}
				}

				//move player on the x axis after collision checks
				x += clamp(hor_speed, -scr_horizontal, scr_horizontal);
				#endregion

				#region Vertical
				//down collision check
				if (ver_speed > 0){
	
					//Grounded Solids---------------------------------------------------------------
					//reset instance for new instance
					insts = noone;
	
					//check for instance
					insts = (instance_place(x,y - abs(ver_speed),obj_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (insts != noone) and (insts.z > z){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x, y - 1,insts))
						{
							y --;
						}
		
						//stop
						ver_speed = -ver_speed/buoyancy;
					}
	
					//Floatingh Solids---------------------------------------------------------------
					//reset instance for new instance
					instf = noone;
	
					//check for instance
					instf = (instance_place(x,y - abs(ver_speed),obj_float_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x, y - 1,instf)){
							y --;
						}
		
						//stop
						ver_speed = -ver_speed/buoyancy;
					}
				}

				// up collision check
				if (ver_speed < 0){
	
					//Grounded Solids---------------------------------------------------------------
					//reset instance for new instance
					insts = noone;
	
					//check for instance
					insts = (instance_place(x,y + abs(ver_speed),obj_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (insts != noone) and (insts.z > z){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x, y + 1,insts)){
							y ++;
						}
		
						//stop
						ver_speed = -ver_speed/buoyancy;
					}
	
					//Floatingh Solids---------------------------------------------------------------
					//reset instance for new instance
					instf = noone;
	
					//check for instance
					instf = (instance_place(x,y + abs(ver_speed),obj_float_solid));
	
					//if instanceexists and is taller than players z stop the player
					if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
						//ensures smooth and consistant collision
						while(!place_meeting(x, y + 1,instf)){
							y ++;
						}
		
						//stop
						ver_speed = -ver_speed/buoyancy;
					}
				}

				//move player on the y axis after collision checks
				y += clamp(-ver_speed, -scr_horizontal, scr_horizontal);
			
				#endregion
		
			#endregion
		
			#region gravity 

			if (zjump){
				inst = noone;
				inst = instance_place(x,y,obj_float_solid);
				if ((inst != noone) and (z < inst.z)){
					if (inst.z-inst.height-height > 0){
						z = clamp(z + zspeed, 0, inst.z-inst.height-height); // z pos goes up
					}
				}
				else{
					z += zspeed;
				}
			}

			//if not ontop of block
			if ((!instance_place(x,y,obj_solid)) and (!instance_place(x,y,obj_float_solid))){
				zfloor = 0; /*zfloor is ground level*/
			}

			//if not on ground
			if (!z <= zfloor){
				z -= zgrav; //apply downforce on z pos
				zgrav += zacce; //rav gets stronger each step
			}

			//stop falling when you hit zfloor
			if (z <= zfloor+1/*+1 for sticking glitch on ground*/){
				zgrav = 0; //stop applying downforce
				z = zfloor;	//nap z pos to on ground
				zjump = false; //no longer in the air
				zspeed = original_zspeed;
				if (state == Test_Player_Steel_Wings.hurt){
					state = Test_Player_Steel_Wings.normal;
					cool_down = false;
					hor_speed = 0;
					ver_speed = 0;
				}
			}

			#endregion
		
		}
		
		#endregion
}