{
    "id": "c5bf00c0-6682-45bc-aca1-a3d9e922a560",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player_wingsofsteel",
    "eventList": [
        {
            "id": "958cbc5d-ebe5-4f48-8b3e-4ff2348442e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "eba794da-1fd8-44f1-989a-df44024f01ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "4232d866-5791-424c-8192-989ebceb23bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "d166bf65-56b8-425e-b56b-d6935cb039f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "cdfdd9df-0964-4e79-96db-9456174bc89b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "4b25fc98-1b9c-4bd8-9671-ddc5f1789236",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "1eb122be-6662-449c-bd92-d118b0c9acd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "7c77a875-4feb-454f-a78a-7ba60d051baa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "e169bb6f-e30a-4dd1-984b-c9cb3313f74f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "7b592078-ec17-40fb-a096-cf079c6bfd84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "000aff8f-29e1-4cc4-a659-a05299056350",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        },
        {
            "id": "bcecd8f5-f8ad-459a-8b65-2e629e07d597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "c5bf00c0-6682-45bc-aca1-a3d9e922a560"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0ad9e91-d1ed-4099-b2d6-2190b44a6c1d",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0372df68-2e6b-4f0f-ae34-c6f77ac43f37",
    "visible": true
}