/// @description Insert description here
if (!cool_down){
	scr_vertical = other.upward_force;
	scr_horizontal = other.forward_force;
	scr_damage = other.strength;
	scr_stun = other.stun;
	scr_dir = other.dir;

	state = Test_Player_Steel_Wings.hurt;
	cool_down = true;
}

instance_destroy(other);