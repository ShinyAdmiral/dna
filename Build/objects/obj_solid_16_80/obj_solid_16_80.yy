{
    "id": "43b52163-48bf-4c7a-a8f9-aeb6407c52a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_16_80",
    "eventList": [
        {
            "id": "edcfe01d-7b64-4ba8-9540-110c79733b89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43b52163-48bf-4c7a-a8f9-aeb6407c52a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aebe0d84-4488-4c6e-9155-210355f0ebff",
    "visible": true
}