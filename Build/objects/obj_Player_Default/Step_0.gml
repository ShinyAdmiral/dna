/// @description State Machine
Depth_Update(0);

switch state {
	case(Player_State.normal):
		#region normal State
	
		move_speed = original_move_speed;

		Player_Input();

		Player_Movement_Calc();

		Player_Z_Movement();
		
		Edge_Teeter();
		
		Player_Collisions_Check();
		
		Player_Normal_Drawing();
		
		#endregion
		
		if (keyboard_check(roll)){
			state = Player_State.rolling;
			roll_anticipate = true;
			roll_timer = true;
		}
		
		break;
	
	case(Player_State.rolling):
		
		Player_Z_Movement();
			
		Player_Rolling();
}