{
    "id": "b32feb26-719b-428a-a0ac-1be31cb4f8a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player_Default",
    "eventList": [
        {
            "id": "883bcad1-1184-4b40-b047-0e781cb2aa37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "93b62da5-4293-4fca-b258-c300cd0212ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "76ad3eac-503b-472f-bac9-c8efa3c18d87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "66009bb1-afe5-42a1-a544-cb98255e5977",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "d4daa63b-c483-406a-bf83-26f104f7d09c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "74a570ea-e191-47a3-bd0d-bea0e0ad8206",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "995d4419-1ccc-4dee-8ab6-d30e46d78614",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        },
        {
            "id": "a1914db7-39af-4ac9-bcb5-a7cc7fdd280c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b32feb26-719b-428a-a0ac-1be31cb4f8a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0ad9e91-d1ed-4099-b2d6-2190b44a6c1d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3636fc3-32a9-4689-b5c5-4fbaa137b986",
    "visible": true
}