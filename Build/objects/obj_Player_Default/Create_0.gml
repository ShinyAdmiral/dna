/// @description Variables
event_inherited();

#region Independent Variables (Change These)
move_speed = 2; //top moving speed
acceleration = .15; //acceleration rate
stopping_power = .75; //deceleration rate
gamepad_dead_zone = .1; //deadzone of the gamepad
zspeed = 3; //speed of jump
height = 29; //height of the player (for ceiling collisions)
roll_speed = 2.5;//speed when rolling
roll_anticipation_lag = 0.05; //anticipation lag time by full seconds
roll_duration = .8; //duration length of the role by full seconds
teeter_time = .5; // time player is left suspended in mid air  (secs)
ledge_run_time = .25;// time player given to leave the edge after execution of teeter (secs)

#endregion

#region Dependent Variables (DO NOT CHANGE)


#region Floor Movement
hor_speed = 0; //horizontal speed
ver_speed = 0; //vertical speed
x_axis = 0; //gamepad x axis 
y_axis = 0; //gamepad y axis 
sped = 0; //used to push acceleration
play_speed = 0; //player calculated speed
dir = 0; //fake direction
original_move_speed = move_speed; //since movespeed changes

#endregion

#region Air Movement
z = 0; //z pos
zfloor = 0; //z pos of floor beneath you
zgrav = 0; //changes, applies downforce
zjump = false; //if in air

#endregion

#region Roll Variables
roll_anticipate = false; //boolean used for anticipation state
roll_timer = false; // boolean used for setting a timer once per roll
rolling = false; // boolean used for rolling state
#endregion

#region Ledge Teeter variables
teeter_up = false; // boolean for instance specific collision
teeter_down = false; // boolean for instance specific collision
teeter_right = false; // boolean for instance specific collision
teeter_left = false; // boolean for instance specific collision
ledge_teeter = true; //boolean with ledge teeter
#endregion

#endregion

#region State Machine
enum Player_State{
	normal,
	rolling
}

state = Player_State.normal;

#endregion