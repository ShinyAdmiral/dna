/// @description State Machine
Depth_Update(0);

switch state {
	case(Test_Player_State.normal):
	
		move_speed = original_move_speed;

		Player_Input();

		Player_Movement_Calc();

		Player_Z_Movement();
		
		Edge_Teeter();
		
		Player_Collisions_Check();
		
		Player_Normal_Drawing();
		
		if (roll){
			state = Test_Player_State.rolling;
			roll_anticipate = true;
			roll_timer = true;
		}
		
		if (mouse_check_button_pressed(mb_left)){
			punch = true;
			state = Test_Player_State.attack;
		}
		
		break;
	
	case(Test_Player_State.rolling):
		
		Player_Z_Movement();
			
		Player_Rolling();
		
		break;
	
	case(Test_Player_State.attack): 
		sprite_index = spr_skeleton_attack_frame;
		
		if (punch){
			alarm[2] = 5;
			var a = instance_create_layer(x+21,y-30, "Hit_Boxes", obj_hitbox_test);
			a.z = z;
			a.dir = dir;
			punch = false;
		}
		
		break;
}