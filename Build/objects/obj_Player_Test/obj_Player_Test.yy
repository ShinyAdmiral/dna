{
    "id": "adeafea9-87ac-4de3-afb4-c6032be5480c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player_Test",
    "eventList": [
        {
            "id": "6048de16-9552-4929-9dcb-098301ca6845",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "7fbc552d-8026-4f33-abdd-aad9b07b33fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "74bb62db-cf13-432a-8b48-9ed0c1daee22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "23ad4e9e-ebcb-458b-b608-b691c0f6e68b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "2806aa1c-f28a-43cf-bb84-e57134e1e69b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "72884940-e7a8-46e6-8198-c5ce6c1e878d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "2e5a2895-5ead-4d38-98a4-dba0d6eeafd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "36939576-983b-4574-b674-7068a925f59e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        },
        {
            "id": "758605ca-2ea5-4cfe-8905-8a9226558623",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "adeafea9-87ac-4de3-afb4-c6032be5480c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0ad9e91-d1ed-4099-b2d6-2190b44a6c1d",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3636fc3-32a9-4689-b5c5-4fbaa137b986",
    "visible": true
}