/// @description Contols

//in game controls
globalvar move_up;
globalvar move_down;
globalvar move_right;
globalvar move_left;
globalvar jump;
globalvar roll;
globalvar attack_1;
globalvar attack_2;

move_up = ord("W");
move_left = ord("A");
move_right = ord("D");
move_down = ord("S");
roll = vk_space;
attack_1 = mb_left;
attack_2 = mb_right;