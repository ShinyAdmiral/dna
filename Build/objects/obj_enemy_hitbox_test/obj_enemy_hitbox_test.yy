{
    "id": "c9a9eed0-7e6d-4213-8516-57b9750e6b46",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_hitbox_test",
    "eventList": [
        {
            "id": "cab35124-fd0b-47a1-a1ed-003badffb06e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9a9eed0-7e6d-4213-8516-57b9750e6b46"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "000aff8f-29e1-4cc4-a659-a05299056350",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0695f1a-5f0a-44f0-874d-3022661d3a3b",
    "visible": true
}