/// @description Insert description here
if (loaded = true){
	var track_pos = audio_sound_get_track_position(bgm);
	
	if (track_pos > track_length){
		audio_sound_set_track_position(bgm, track_pos - loop_length);
		
		//if (combat_music != noone)
			audio_sound_set_track_position(bgm1, track_pos - loop_length);
		
		//if (low_health_music != noone)
			audio_sound_set_track_position(bgm2, track_pos - loop_length);
		
		//if (atmospheric_music != noone)
			audio_sound_set_track_position(bgm3, track_pos - loop_length);
		
		if (victory_music != noone)
			audio_sound_set_track_position(bgm4, track_pos - loop_length);
	}
	
	Adaptive_Music_Check(combat_music, combat);
	
	Adaptive_Music_Check(low_health_music, low_health);
	
	Adaptive_Music_Check(atmospheric_music, atmospheric);
	
	Adaptive_Music_Check(victory_music, victory);
	
}

//make sure to remove

if (keyboard_check_pressed(ord("1"))){
	if (combat)
		combat = false;
	else if (!combat)
		combat = true;
}

if (keyboard_check_pressed(ord("2"))){
	if (low_health)
		low_health = false;
	else if (!low_health)
		low_health = true;
}

if (keyboard_check_pressed(ord("3"))){
	if (atmospheric)
		atmospheric = false;
	else if (!atmospheric)
		atmospheric = true;
}
