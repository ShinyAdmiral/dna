/// @description Insert description here
bgm = audio_play_sound(level_music, 100, false);
audio_sound_gain(bgm, max_volume, 0);

bgm1 = audio_play_sound(level_music, 100, false);
audio_sound_gain(bgm1, 0, 0);
bgm2 = audio_play_sound(level_music, 100, false);
audio_sound_gain(bgm2, 0, 0);
bgm3 = audio_play_sound(level_music, 100, false);
audio_sound_gain(bgm3, 0, 0);
bgm4 = audio_play_sound(level_music, 100, false);
audio_sound_gain(bgm4, 0, 0);


if (combat_music != noone){
	audio_stop_sound(bgm1);
	bgm1 = audio_play_sound(combat_music, 100, false);
	audio_sound_gain(combat_music, 0, 0);
}
else{
	audio_stop_sound(bgm1);
}

if (low_health_music != noone){
	audio_stop_sound(bgm2);
	bgm2 = audio_play_sound(low_health_music , 100, false);
	audio_sound_gain(low_health_music, 0, 0);
}
else{
	audio_stop_sound(bgm2);
}

if (atmospheric_music != noone){
	audio_stop_sound(bgm3);
	bgm3 = audio_play_sound(atmospheric_music, 100, false);
	audio_sound_gain(atmospheric_music, 0, 0);
}
else{
	audio_stop_sound(bgm3);
}

if (victory_music != noone){
	audio_stop_sound(bgm4);
	bgm4 = audio_play_sound(victory_music, 100, false);
	audio_sound_gain(victory_music, 0, 0);
}
else{
	audio_stop_sound(bgm4);
}

track_length = intro_length + loop_length;
loaded = true;