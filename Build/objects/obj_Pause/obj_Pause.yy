{
    "id": "367b4090-77f3-4ddf-aaa8-8fbe4e556900",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Pause",
    "eventList": [
        {
            "id": "87e82f85-1d55-4a5f-a4cc-a7fb4fc4cbe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "367b4090-77f3-4ddf-aaa8-8fbe4e556900"
        },
        {
            "id": "04219f38-2782-4748-a06a-9d194d4fc40d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "367b4090-77f3-4ddf-aaa8-8fbe4e556900"
        },
        {
            "id": "b3cf4bc2-30dc-4ff2-bee1-125bf9e86e1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "367b4090-77f3-4ddf-aaa8-8fbe4e556900"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}