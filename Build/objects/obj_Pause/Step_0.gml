if (instance_exists(obj_Camera)) {
	menu_x = obj_Camera.cx + obj_Camera.center_w;
	menu_y = obj_Camera.cy + obj_Camera.center_h;
}

//switching buttons
move_down_pause = keyboard_check_pressed(move_down);
move_up_pause = keyboard_check_pressed(move_up);

menu_move = move_down_pause - move_up_pause;
menu_index += menu_move;
if(menu_index < 0)
	menu_index = buttons-1;
	
if(menu_index > buttons-1)
	menu_index = 0;
	
/* FOR SELECTION CHANGE Noise
menu_index!= last_selected) audio_play_sound(name,1,false)
*/

last_selected = menu_index;

//Menu buttons

if (menu_index == 0 && keyboard_check_pressed(vk_enter)) {
	global.pause = 0;
	for(i = 0; i < ds_list_size(deactivate_list); i++){
			instance_activate_object(deactivate_list[| i]);
	}
}

if (menu_index == 1 && keyboard_check_pressed(vk_enter)) {
	game_end();
}

if keyboard_check_pressed(vk_escape){
	if (global.pause == 0) {
		global.pause = 1;
		for(i = 0; i < ds_list_size(deactivate_list); i++){
			instance_deactivate_object(deactivate_list[| i]);
		}
	}
	else {
		global.pause = 0;
		for(i = 0; i < ds_list_size(deactivate_list); i++){
			instance_activate_object(deactivate_list[| i]);
		}
	}	
}