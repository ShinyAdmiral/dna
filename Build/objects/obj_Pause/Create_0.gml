depth = -9999

global.pause = 0;

menu_x = 0;
menu_y = 0;

button_h = 75;


// button sprites
button[0] = spr_continue;
button[1] = spr_quit;
buttons = array_length_1d(button);

menu_index = 0;
last_selected = 0;

deactivate_list = ds_list_create();
ds_list_add(deactivate_list, obj_Camera, obj_player, obj_player_hitbox, obj_Enemy, obj_enemy_hitbox);