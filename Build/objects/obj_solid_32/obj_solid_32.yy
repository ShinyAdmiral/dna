{
    "id": "322359ed-71fe-4e4d-944d-83d78545f0c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_32",
    "eventList": [
        {
            "id": "27091b8b-b7e3-41a2-95c8-e7869d5b47aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "322359ed-71fe-4e4d-944d-83d78545f0c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7ba047a3-b9e2-4600-b5c8-963701c4d05c",
    "visible": true
}