{
    "id": "c2346d49-cc38-4b3d-8fc5-0c6e5bf3ba8b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object18",
    "eventList": [
        {
            "id": "a0a93903-90ee-4ecf-9eac-d0c4a434bd95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c2346d49-cc38-4b3d-8fc5-0c6e5bf3ba8b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33324bb3-6106-49c6-956c-404da60fade4",
    "visible": true
}