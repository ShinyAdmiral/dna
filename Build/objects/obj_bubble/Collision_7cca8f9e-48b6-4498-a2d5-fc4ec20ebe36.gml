/// @description Insert description here
if (bub_id > other.bub_id){
	if (!cool){
		if (speed != 0){
			other_dir = point_direction(x, y, other.x, other.y);
						
			other_speed_ratio = 1 - (abs(other_dir) mod 90)/90;
			this_speed_ratio = 1 - other_speed_ratio;
		
			total_speed = speed;
			show_debug_message("initial direction: " + string(direction))
			show_debug_message("colllision dir: " + string(other_dir))
			show_debug_message("speed_ratio: " + string(other_speed_ratio))
			
			show_debug_message("hspeed: " + string(hspeed))
			show_debug_message("vspeed: " + string(vspeed))
			
			
			if (other_dir div 180 > 0 and hspeed > 0){
				other.direction = direction - abs(other_dir mod 90 - 90);
				direction += abs(other_dir) mod 90;
				temp = other_speed_ratio;
				other_speed_ratio = this_speed_ratio;
				this_speed_ratio = temp;
			}
			else if (other_dir div 180 == 0 and hspeed < 0){
				other.direction = direction - abs(other_dir mod 90 - 90);
				direction += abs(other_dir) mod 90;
				temp = other_speed_ratio;
				other_speed_ratio = this_speed_ratio;
				this_speed_ratio = temp;
			}
			else{
				other.direction = direction + abs(other_dir mod 90);
				direction -= abs(other_dir) mod 90;
			}
			
			//quadrant = other_dir div 90 + 1;	
			
			//show_debug_message("quadrant: " + string(quadrant))
			
			//switch(quadrant){
			//case(1):
			//	if (vspeed != 0 && hspeed = 0){
			//		other.direction = direction - abs(other_dir mod 90 - 90);
			//		direction += abs(other_dir) mod 90;
			//	}
			//	else if (vspeed != 0 && hspeed != 0){
			//		other.direction = direction + abs(other_dir) mod 90;
			//		direction -= abs(other_dir) mod 90;
			//	}
			//	else{
			//		other.direction = direction + abs(other_dir) mod 90;
			//		direction -= abs(other_dir) mod 90;
			//	}
			//	break;
			
			//case(2):
			//	if (vspeed != 0 && hspeed = 0){
			//		other.direction = direction + abs(other_dir) mod 90;
			//		direction-= abs(other_dir) mod 90;
			//	}
			//	else if (vspeed != 0 && hspeed != 0){
			//		if (hspeed < 0){
			//			other.direction = direction +  abs(other_dir) mod 90;
			//			direction -=  abs(other_dir) mod 90;
			//		}
			//		else{
			//			other.direction = direction + 2* abs(other_dir) mod 90;
			//			direction -= 2 * abs(other_dir) mod 90;
			//			temp = other_speed_ratio;
			//			other_speed_ratio = this_speed_ratio;
			//			this_speed_ratio = temp;
			//		}
			//	}
			//	else{
			//		other.direction = direction - abs(other_dir mod 90 - 90);
			//		direction += abs(other_dir) mod 90;
			//	}
			//	break;
			
			//case(3):
			//	if (vspeed != 0 && hspeed = 0){
			//		other.direction = direction - abs(other_dir mod 90 - 90);
			//		direction += abs(other_dir) mod 90;
			//	}
			//	else if (vspeed != 0 && hspeed != 0){
			//		other.direction = direction - abs(other_dir mod 90 - 90);
			//		direction += abs(other_dir) mod 90;
			//	}
			//	else{
			//		other.direction = direction + abs(other_dir) mod 90;
			//		direction -= abs(other_dir) mod 90;
			//	}
			//	break;
			
			//case(4):
			//	if (vspeed != 0 && hspeed = 0){
			//		other.direction = direction + abs(other_dir) mod 90;
			//		direction-= abs(other_dir) mod 90;
			//	}
			//	else if (vspeed != 0 && hspeed != 0){
			//		other.direction = direction + abs(other_dir) mod 90;
			//		direction -= abs(other_dir) mod 90;
			//	}
			//	else{
			//		other.direction = direction - abs(other_dir mod 90 - 90);
			//		direction += abs(other_dir) mod 90;
			//	}
			//	break;
			//}
			
			show_debug_message("other.direction: " + string(other.direction))
			show_debug_message("direction: " + string(direction))
	
			other.speed = total_speed * other_speed_ratio;
			speed = total_speed * this_speed_ratio;
	
			show_debug_message("other.speed: " + string(other.speed))
			show_debug_message("speed: " + string(speed))
		
			cool = true;
	
			show_debug_message("------------------------------------\n\n")
		}
	}
}