{
    "id": "8befb266-df0c-48d9-a5db-e281c7cce6b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bubble",
    "eventList": [
        {
            "id": "b19ceae7-8969-4fe2-aa55-cbd9d44cbaff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8befb266-df0c-48d9-a5db-e281c7cce6b2"
        },
        {
            "id": "728ccd5b-7b9c-493e-ab7a-abe22ffa4018",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8befb266-df0c-48d9-a5db-e281c7cce6b2"
        },
        {
            "id": "5143948d-803e-4a5f-a57b-04bff5397829",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8befb266-df0c-48d9-a5db-e281c7cce6b2"
        },
        {
            "id": "f06d989f-064a-41c5-86a9-0ea46c127931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8befb266-df0c-48d9-a5db-e281c7cce6b2"
        },
        {
            "id": "77f7426a-cf45-4570-8130-bdd57392bd41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8befb266-df0c-48d9-a5db-e281c7cce6b2"
        },
        {
            "id": "7cca8f9e-48b6-4498-a2d5-fc4ec20ebe36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8befb266-df0c-48d9-a5db-e281c7cce6b2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8befb266-df0c-48d9-a5db-e281c7cce6b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "33186289-f689-4bde-810b-d7e5ad812616",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "bub_id",
            "varType": 0
        },
        {
            "id": "a8955f66-af10-4b68-9c8f-a922194adc2e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "z",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "51dd8d6a-1ae9-4ec0-a713-1e0f45060d15",
    "visible": true
}