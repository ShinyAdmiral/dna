/// @description Menu Functionality and Visuals
if(in_menu){
	menu_up = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(0, gp_padu);
	menu_down = keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down) || gamepad_button_check_pressed(0, gp_padd);
	menu_enter = keyboard_check_pressed(vk_enter) || keyboard_check_pressed(ord("E")) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, gp_face1) || mouse_check_button_pressed(mb_left);
	menu_escape = keyboard_check_pressed(vk_escape) || keyboard_check_pressed(ord("Q")) || keyboard_check_pressed(vk_backspace) || gamepad_button_check_pressed(0, gp_face2) || gamepad_button_check_pressed(0, gp_start);
	
	if (menu_down)
		pointer[0]++;
	else if (menu_up)
		pointer[0]--;
	
	gamepad_set_axis_deadzone(0,.8);
	menu_up_joy = -gamepad_axis_value(0,gp_axislv);
	menu_down_joy = gamepad_axis_value(0,gp_axislv);

	if (menu_up_joy && !joy_stick_up_cool){
		instance_destroy(obj_Menu_Cursor);
		mouse_previous_x = mouse_x;
		mouse_previous_y = mouse_y;
		pointer[0]--;
		joy_stick_up_cool = true;
		joy_stick_down_cool = false;
		alarm[0] = room_speed / 6;
	}
	if (menu_down_joy && !joy_stick_down_cool){
		instance_destroy(obj_Menu_Cursor);
		mouse_previous_x = mouse_x;
		mouse_previous_y = mouse_y;
		pointer[0]++;
		joy_stick_up_cool = false;
		joy_stick_down_cool = true;
		alarm[1] = room_speed / 6;
	}


	if (pointer[0] > max_options[0]){
		pointer[0] = 1;
	}
	else if (pointer[0] < 1){
		pointer[0] = max_options[0];
	}
	
	if ((menu_up || menu_down || menu_enter|| menu_escape) && !first){
		instance_destroy(obj_Menu_Cursor);
		mouse_previous_x = mouse_x;
		mouse_previous_y = mouse_y;
	}
	
	if (instance_exists(obj_Menu_Cursor)){
		if (obj_Menu_Cursor.y < room_height/2){
			pointer[0] = 1;
		}
		else if (obj_Menu_Cursor.y < room_height/2+32){
			pointer[0] = 2;
		}
		else if (obj_Menu_Cursor.y < room_height/2+64){
			pointer[0] = 3;
		}
	}
	else{
		if ((mouse_previous_x != mouse_x) || (mouse_previous_y != mouse_y)){
			instance_create_depth(mouse_x, mouse_y, -3000, obj_Menu_Cursor);
		}
	}
	
	if (menu_escape && !first){
		active = false;
		escape = false;
		screen_saved = true;
		pointer[0] = 1;
		in_menu = false;
		instance_destroy(obj_Menu_Cursor)
		instance_activate_layer("Instances");
	}

	if (!first){
	switch (pointer[0]){
		case 1:
			draw_set_color(c_blue);
			draw_text(room_width/2-32, room_height/2-32, "Back...");
			draw_set_color(c_yellow);
			draw_text(room_width/2-32, room_height/2, "Keyboard");
			draw_text(room_width/2-32, room_height/2+32, "Gamepad");
			if (menu_enter){
				obj_Menu.in_menu = true;
				instance_destroy(self)
			}
			break;
		
		case 2:
			draw_set_color(c_yellow);
			draw_text(room_width/2-32, room_height/2-32, "Back...");
			draw_set_color(c_blue);
			draw_text(room_width/2-32, room_height/2, "Keyboard");
			draw_set_color(c_yellow);
			draw_text(room_width/2-32, room_height/2+32, "Gamepad");
			if (menu_enter){
				in_menu = false;
			}
			break;
	
		case 3:
			draw_set_color(c_yellow);
			draw_text(room_width/2-32, room_height/2-32, "Back...");
			draw_text(room_width/2-32, room_height/2, "Keyboard");
			draw_set_color(c_blue);
			draw_text(room_width/2-32, room_height/2+32, "Gamepad");
			if (menu_enter){
				in_menu = false;
			}
			break;
		}
	}
	first = false;
}

//Controller Menu ---------------------------------------------------------------------------------------------------
else{
	if (controller_menu){
		menu_up = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(0, gp_padu);
		menu_down = keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down) || gamepad_button_check_pressed(0, gp_padd);
		menu_enter = keyboard_check_pressed(vk_enter) || keyboard_check_pressed(ord("E")) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, gp_face1) || mouse_check_button_pressed(mb_left);
		menu_escape = keyboard_check_pressed(vk_escape) || keyboard_check_pressed(ord("Q")) || keyboard_check_pressed(vk_backspace) || gamepad_button_check_pressed(0, gp_face2) || gamepad_button_check_pressed(0, gp_start);
	
		if (menu_down)
			pointer[1]++;
		else if (menu_up)
			pointer[1]--;
	
		gamepad_set_axis_deadzone(0,.8);
		menu_up_joy = -gamepad_axis_value(0,gp_axislv);
		menu_down_joy = gamepad_axis_value(0,gp_axislv);

		if (menu_up_joy && !joy_stick_up_cool){
			instance_destroy(obj_Menu_Cursor);
			mouse_previous_x = mouse_x;
			mouse_previous_y = mouse_y;
			pointer[1]--;
			joy_stick_up_cool = true;
			joy_stick_down_cool = false;
			alarm[0] = room_speed / 6;
		}
		if (menu_down_joy && !joy_stick_down_cool){
			instance_destroy(obj_Menu_Cursor);
			mouse_previous_x = mouse_x;
			mouse_previous_y = mouse_y;
			pointer[1]++;
			joy_stick_up_cool = false;
			joy_stick_down_cool = true;
			alarm[1] = room_speed / 6;
		}


		if (pointer[1] > max_options[1]){
			pointer[1] = 1;
		}
		else if (pointer[1] < 1){
			pointer[1] = max_options[1];
		}
	
		if ((menu_up || menu_down || menu_enter|| menu_escape) && !first){
			instance_destroy(obj_Menu_Cursor);
			mouse_previous_x = mouse_x;
			mouse_previous_y = mouse_y;
		}
	
		if (instance_exists(obj_Menu_Cursor)){
			if (obj_Menu_Cursor.y < room_height/2){
				pointer[1] = 1;
			}
			else if (obj_Menu_Cursor.y < room_height/2+32){
				pointer[1] = 2;
			}
			else if (obj_Menu_Cursor.y < room_height/2+64){
				pointer[1] = 3;
			}
		}
		else{
			if ((mouse_previous_x != mouse_x) || (mouse_previous_y != mouse_y)){
				instance_create_depth(mouse_x, mouse_y, -3000, obj_Menu_Cursor);
			}
		}
	
		if (menu_escape && !first){
			active = false;
			escape = false;
			screen_saved = true;
			pointer[1] = 1;
			in_menu = false;
			instance_destroy(obj_Menu_Cursor)
			instance_activate_layer("Instances");
		}

		if (!first){
		switch (pointer[1]){
			case 1:
				draw_set_color(c_blue);
				draw_text(room_width/2-32, room_height/2-32, "Back...");
				draw_set_color(c_yellow);
				draw_text(room_width/2-32, room_height/2, "Keyboard");
				draw_text(room_width/2-32, room_height/2+32, "Gamepad");
				if (menu_enter){
					obj_Menu.in_menu = true;
					instance_destroy(self)
				}
				break;
		
			case 2:
				draw_set_color(c_yellow);
				draw_text(room_width/2-32, room_height/2-32, "Back...");
				draw_set_color(c_blue);
				draw_text(room_width/2-32, room_height/2, "Keyboard");
				draw_set_color(c_yellow);
				draw_text(room_width/2-32, room_height/2+32, "Gamepad");
				if (menu_enter){
					in_menu = false;
				}
				break;
	
			case 3:
				draw_set_color(c_yellow);
				draw_text(room_width/2-32, room_height/2-32, "Back...");
				draw_text(room_width/2-32, room_height/2, "Keyboard");
				draw_set_color(c_blue);
				draw_text(room_width/2-32, room_height/2+32, "Gamepad");
				if (menu_enter){
					in_menu = false;
				}
				break;
			}
		}
		first = false;
	}
	
	
	if(keyboard_menu){
		
	}
}