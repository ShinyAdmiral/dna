{
    "id": "254059e9-72c2-443d-896a-e8c333773fe4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Controls_Menu",
    "eventList": [
        {
            "id": "4704a341-0fa9-43b3-be46-bdb8917aed2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "254059e9-72c2-443d-896a-e8c333773fe4"
        },
        {
            "id": "792ca3ff-319a-4853-bc74-b5671273339a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "254059e9-72c2-443d-896a-e8c333773fe4"
        },
        {
            "id": "bd06c070-a32f-49c2-88fc-88e7ed4cfc02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "254059e9-72c2-443d-896a-e8c333773fe4"
        },
        {
            "id": "3dff7312-be45-470e-b3bb-147c6a9e4e91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "254059e9-72c2-443d-896a-e8c333773fe4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0543934f-9fe6-4bd1-9707-543d532a19a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}