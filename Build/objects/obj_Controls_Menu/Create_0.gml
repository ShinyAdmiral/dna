/// @description menu variables
pointer[0] = 1;
max_options[0] = 3;
pointer[1] = 1;
max_options[1] = 3;
pointer[2] = 1;
max_options[2] = 3;
in_menu = true;
spr_pause = spr_solid;
first = true;
mouse_previous_x = 0;
mouse_previous_y = 0;
joy_stick_up_cool = false;
joy_stick_down_cool = false;
controller_menu = false;
keyboard_menu = false;