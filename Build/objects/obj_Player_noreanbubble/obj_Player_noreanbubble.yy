{
    "id": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player_noreanbubble",
    "eventList": [
        {
            "id": "49973f39-22cd-48c2-bbd7-ffc3a914289f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "41ef03d9-8043-4340-a07b-0b9b7820671e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "2164964f-7748-4cf0-b4ad-f4af593ed3c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "b2b1f5ea-7c92-437c-a202-35f83d26952e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "c56439c9-e0bc-4048-be59-3232820b4eb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "28bf56cd-4fb0-4888-856a-e3dc84900346",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "6d11ceff-9c60-46ee-8036-dc44b6b23595",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "71e27c0a-1234-4a17-a7e3-3abc0f8bcf18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "14268d46-fff9-412b-8d8d-2d8fd17ca2fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "29ff7fab-48dd-4a23-b74f-16164893528a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "000aff8f-29e1-4cc4-a659-a05299056350",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        },
        {
            "id": "064c483d-a8ab-45b0-bcb4-255017fec615",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0ad9e91-d1ed-4099-b2d6-2190b44a6c1d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
    "visible": true
}