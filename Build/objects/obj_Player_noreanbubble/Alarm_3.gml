/// @description Burst Timer
if (burst){
	if (burst_activated){
		burst_count ++;
		alarm[3] = burst_max_charge/max_burst_ammount * room_speed;
	}

	if (!burst_activated){
		burst_activated = true;
		burst_count = 3;
		alarm[3] = burst_max_charge/max_burst_ammount * room_speed;
	}
}