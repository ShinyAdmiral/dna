/// @description Insert description here
ran = random_range(burst_speed_min, burst_speed_max);
					
if (reverse){
	burst_dir += random_range(0, angle_offset);
	reverse = false;
}
else{
	burst_dir -= random_range(0, angle_offset);
	reverse = true;
}
					
Bubble_Create(burst_dir, ran);
burst_index++;

if(burst_index < burst_count)
{
	alarm[4] = fire_rate * room_speed;
}

else{
	burst_count = 0;
	state = Player_State_Norean_Bubble.normal;
}