/// @description Ledge Timers

//ledge guard reset (Lets the user go)
if (!ledge_teeter){
	ledge_teeter = true;
	teeter_up = false;
	teeter_down = false;
	teeter_right = false;
	teeter_left = false;
}

//Starts timer for ledge teeter (Resets if player leaves the hit box)
else{
	if (teeter_up){
		ledge_teeter = false;
		alarm[1] = room_speed * ledge_run_time;
	}

	if (teeter_down){
		ledge_teeter = false;
		alarm[1] = room_speed * ledge_run_time;
	}

	if (teeter_right){
		ledge_teeter = false;
		alarm[1] = room_speed * ledge_run_time;
	}

	if (teeter_left){
		ledge_teeter = false;
		alarm[1] = room_speed * ledge_run_time;
	}
}