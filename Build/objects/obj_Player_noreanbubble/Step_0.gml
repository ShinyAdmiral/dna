/// @description State Machine
Depth_Update(0);

switch state {
	case(Player_State_Norean_Bubble.normal):
		#region normal State
	
		move_speed = original_move_speed;

		Player_Input();

		Player_Movement_Calc();

		Player_Z_Movement();
		
		Edge_Teeter();
		
		Player_Collisions_Check();
		
		Player_Normal_Drawing();
		
		#endregion
		
		#region change state
		
		if (keyboard_check_pressed(roll)){
			state = Player_State_Norean_Bubble.rolling;
			roll_anticipate = true;
			roll_timer = true;
		}
		
		if (mouse_check_button_pressed(attack_2)){
			state = Player_State_Norean_Bubble.trap;
			burst = true;
			burst_activated = false;
			burst_dir = dir;
			bubble_shot_done = false;
			burst_index = 0;
			reverse = false;
			alarm[3] = burst_timer * room_speed;
		}
		
		#endregion
		
		break;
	
	case(Player_State_Norean_Bubble.rolling):
		
		Player_Z_Movement();
			
		Player_Rolling();
		
		break;
	
	case(Player_State_Norean_Bubble.trap):
		#region Bubble Trap
		if (!bubble_shot_done){
			if (mouse_check_button_released(attack_2)){
				if (!burst_activated){
					burst = false;
					Bubble_Create(0,0);
				
					state = Player_State_Norean_Bubble.normal;
				}
				else{
					burst = false;
					bubble_shot_done = true
					alarm[4] = 1;
				}
			}
		
			if (burst_count >= max_burst_ammount)
			{
				burst_count = max_burst_ammount;
				burst = false;
				bubble_shot_done = true
				alarm[4] = 1;
			}
		}
		#endregion
		
		break;
		
}

if mouse_check_button_pressed(mb_left){
	var a = instance_create_depth(x,y,0,obj_bubble);
	a.bub_id = 100;
	a.direction = dir;
	a.speed = burst_speed_min;
}