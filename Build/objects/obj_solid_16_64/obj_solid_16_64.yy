{
    "id": "7bc533b7-b0c0-4429-9a52-9292eba48218",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_16_64",
    "eventList": [
        {
            "id": "25656ac6-d6f0-4576-9486-96021b207db5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7bc533b7-b0c0-4429-9a52-9292eba48218"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54368237-d345-4c69-9c03-45d6da3c6f65",
    "visible": true
}