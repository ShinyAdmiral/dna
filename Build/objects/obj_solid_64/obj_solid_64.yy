{
    "id": "00e696fd-05c0-4155-b68f-9f9aa4cc26a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_64",
    "eventList": [
        {
            "id": "52e2d87d-76cf-4208-b978-aef4e31f490b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00e696fd-05c0-4155-b68f-9f9aa4cc26a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2d299b7-0077-42a6-90e9-eeb7cb5d0656",
    "visible": true
}