{
    "id": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_impass",
    "eventList": [
        {
            "id": "f6af699e-9e90-4ec7-b615-b381be0fe6be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5da5070-4dde-48f2-9db5-8df3d24000e4",
    "visible": true
}