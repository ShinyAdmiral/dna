{
    "id": "4ee68b0c-b6ad-4df4-8669-dacb81c5e36e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_16",
    "eventList": [
        {
            "id": "13e68057-a59a-47c5-8ab1-c496a53a694e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ee68b0c-b6ad-4df4-8669-dacb81c5e36e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fcf01fe5-c481-4057-a2fc-28adf74f7a1f",
    "visible": true
}