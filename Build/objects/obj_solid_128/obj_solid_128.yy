{
    "id": "671d9c0f-9336-4c6d-9b99-52a299603eb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_128",
    "eventList": [
        {
            "id": "8883538f-8bc0-4143-8c93-e14fc1cf3603",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "671d9c0f-9336-4c6d-9b99-52a299603eb3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3c7d6268-74cc-4440-bd0a-d2ef26681423",
    "visible": true
}