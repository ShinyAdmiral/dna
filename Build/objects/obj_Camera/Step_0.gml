/// @description Insert description here

if (instance_exists(obj_player)){
	view_w = camera_get_view_width(view_camera[0]);
	view_h = camera_get_view_height(view_camera[0]);
	center_w = view_w/2;
	center_h = view_h/2;
	distx = 0;
	disty = 0;

	cx = camera_get_view_x(view_camera[0]);
	cy = camera_get_view_y(view_camera[0]);

	if (CamState == CamMode.Follow_Player){
		cx = lerp(cx + center_w, obj_player.x, lerp_power) - center_w;
		cy = lerp(cy+ center_h, obj_player.y, lerp_power) - center_h;
	}

	//if (state == CamMode.Scenic){
	//	if (!gamepad_is_connected(0)){
	//		look_ahead_x = look_ahead;
	//		look_ahead_y = floor(view_h / view_w * look_ahead);
		
	//		if (obj_player.dir <= 45){
	//			if (xdir == -1){
	//				cx = lerp(cx + center_w, obj_player.x + look_ahead_x, lerp_power) - center_w;
	//				cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
	//			}
			
	//			cx = lerp(cx + center_w, obj_player.x + look_ahead_x, lerp_power) - center_w;
	//			cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
			
	//			xdir = 1;
	//			ydir = 0;
	//		}
	//		else if (obj_player.dir < 135){
	//			cx = lerp(cx + center_w, obj_player.x, lerp_power) - center_w;
	//			cy = lerp(cy + center_h, obj_player.y - look_ahead_y, lerp_power) - center_h;
			
	//			ydir = -1;
	//			xdir = 0;
	//		}
	//		else if (obj_player.dir <= 225){
	//			cx = lerp(cx + center_w, obj_player.x - look_ahead_x, lerp_power) - center_w;
	//			cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
			
	//			xdir = -1;
	//			ydir = 0;
	//		}
	//		else if (obj_player.dir < 315){
	//			cx = lerp(cx + center_w, obj_player.x, lerp_power) - center_w;
	//			cy = lerp(cy + center_h, obj_player.y + look_ahead_y, lerp_power) - center_h;
			
	//			ydir = 1;
	//			xdir = 0;
	//		}
	//		else{
	//			cx = lerp(cx + center_w, obj_player.x + look_ahead_x, lerp_power) - center_w;
	//			cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
			
	//			xdir = 1;
	//			ydir = 0;
	//		}
		
			//if (obj_player.dir <= 45){
			//	cx = lerp(cx + center_w, obj_player.x + look_ahead_x, lerp_power) - center_w;
			//	cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
			//}
			//else if (obj_player.dir < 135){
			//	cx = lerp(cx + center_w, obj_player.x, lerp_power) - center_w;
			//	cy = lerp(cy + center_h, obj_player.y - look_ahead_y, lerp_power) - center_h;
			//}
			//else if (obj_player.dir <= 225){
			//	cx = lerp(cx + center_w, obj_player.x - look_ahead_x, lerp_power) - center_w;
			//	cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
			//}
			//else if (obj_player.dir < 315){
			//	cx = lerp(cx + center_w, obj_player.x, lerp_power) - center_w;
			//	cy = lerp(cy + center_h, obj_player.y + look_ahead_y, lerp_power) - center_h;
			//}
			//else{
			//	cx = lerp(cx + center_w, obj_player.x + look_ahead_x, lerp_power) - center_w;
			//	cy = lerp(cy + center_h, obj_player.y, lerp_power) - center_h;
			//}
		//}
	//}

	cx = clamp(cx, 0, room_width - view_w);
	cy = clamp(cy, 0, room_height - view_h);

	camera_set_view_pos(view_camera[0], cx, cy);
}