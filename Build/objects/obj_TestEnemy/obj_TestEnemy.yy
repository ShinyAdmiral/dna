{
    "id": "1e253367-3e46-48a1-b995-bd58d0d4d156",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_TestEnemy",
    "eventList": [
        {
            "id": "d785648f-7859-465a-9996-fdcaf8ed80c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "5f0f2ea4-82a4-48ba-b0e9-d5cc0e976863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "aaa59e2c-f169-42b9-b86b-c745ed7a6d20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "a5f597eb-a76c-4457-b027-9f99d704d012",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "b4d00c8c-d905-483d-b4b2-e97574869057",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "abbe4dfe-aa7a-42fb-8a65-ed6742eb241d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "44506875-29f1-4dc0-ac62-add6a48181d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        },
        {
            "id": "f10da6a5-c31d-421d-8286-cf89ca6d28c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "062683e5-c194-400d-a386-86524f8038b2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1e253367-3e46-48a1-b995-bd58d0d4d156"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fad5264e-121f-447a-bfe7-bac8c95bc88f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a41cb783-87e9-4ee4-9565-b2c3d67c3e7e",
    "visible": true
}