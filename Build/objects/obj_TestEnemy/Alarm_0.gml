/// @description Insert description here
if (state == Test_Enemy_State.normal){
	count = 0;
	found = false;

	#region loop this until path found
	while (true){
		dir = random_range(0, 359);
		rad = degtorad(dir);
	
		x_distance = sin(rad) * max_check_distance;
		y_distance = cos(rad) * max_check_distance;
	
		solid_obj = collision_line(x, y, x_distance, y_distance, obj_solid, true, true)
		if (solid_obj != noone){
			if (z < solid_obj.z){
				found = true;
			}
		}
	
		solid_float = collision_line(x, y, x_distance, y_distance, obj_float_solid, true, true)
		if (solid_float != noone){
			if(z < solid_float.z && z > solid_float.z - solid_float.height){
				found = true;
			}
		}
	
		if (!found){
			break;
		}
	
		if (count == max_loop){
			rad = degtorad(dir + 180);
			x_distance = sin(rad) * max_check_distance;
			y_distance = cos(rad) * max_check_distance;
			break;
		}
	
		count++;
	}
	#endregion

	hor_speed = (x_distance/max_check_distance) * max_speed;
	ver_speed = (y_distance/max_check_distance) * max_speed;

	move = true;

	walk_time = random_range(walk_time_min, walk_time_max);

	alarm[1] = walk_time * room_speed;
}