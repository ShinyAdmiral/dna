/// @description Insert description here

#region Independant Variables
hp = 8;
weight = 1.2;
calc_move_delay_min = 1;
calc_move_delay_max = 3;
walk_time_min = 1;
walk_time_max = 3;
max_check_distance = 48;
max_loop = 10;
max_speed = 1;
max_seeing_distance = 192;
#endregion

#region Dependant Variables
calc_next_move = true;
move = false;
zfloor = 0;
z = 0;
hor_speed = 0;
ver_speed = 0;
zjump = false;
zgrav = 0;
zspeed = 0;
knockback_first = false;

///THESE ARE IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//independant
buoyancy = 2;

//dependent
scr_vertical = 0;
scr_horizontal = 0;
scr_damage = 0;
scr_stun = true;
scr_dir = 0;
cool_down = false;

#endregion

enum Test_Enemy_State{
	normal,
	agro,
	attack,
	stun,
	knockback
}

state = Test_Enemy_State.normal;