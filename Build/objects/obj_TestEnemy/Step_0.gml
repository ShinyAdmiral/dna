/// @description Insert description here
Depth_Update(0);

switch (state){
	case Test_Enemy_State.normal:
		#region normal state
		if (calc_next_move){
			random_delay = random_range(calc_move_delay_min, calc_move_delay_max)
			alarm[0] = random_delay  * room_speed
			calc_next_move = false;
			cool_down = false;
		}
		
		if (move){
			// Collisions and Movement

			#region Horizontal
			//right collision check
			if (hor_speed > 0){
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x + abs(hor_speed),y,obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1,y,insts)){
						x ++;
					}
		
					//stop
					hor_speed = 0;
					move = false;
					calc_next_move = true;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x + abs(hor_speed),y,obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if ((instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height)){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1,y,instf))
					{
						x ++;
					}
		
					//stop
					hor_speed = 0;
					move = false;
					calc_next_move = true;
				}
			}

			//left collision check
			if (hor_speed < 0){
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place((x - abs(hor_speed)),y,obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1,y,insts))
					{
						x --;
					}
		
					//stop
					hor_speed = 0;
					move = false;
					calc_next_move = true;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x - abs(hor_speed),y,obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1,y,instf))
					{
						x --;
					}
		
					//stop
					hor_speed = 0;
					move = false;
					calc_next_move = true;
				}
			}

			//move player on the x axis after collision checks
			x += clamp(hor_speed, -max_speed, max_speed);
			#endregion

			#region Vertical
			//down collision check
			if (ver_speed > 0){
	
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x,y - abs(ver_speed),obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1,insts))
					{
						y --;
					}
		
					//stop
					ver_speed = 0;
					move = false;
					calc_next_move = true;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x,y - abs(ver_speed),obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1,instf)){
						y --;
					}
		
					//stop
					ver_speed = 0;
					move = false;
					calc_next_move = true;
				}
			}

			// up collision check
			if (ver_speed < 0){
	
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x,y + abs(ver_speed),obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y + 1,insts)){
						y ++;
					}
		
					//stop
					ver_speed = 0;
					move = false;
					calc_next_move = true;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x,y + abs(ver_speed),obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y + 1,instf)){
						y ++;
					}
		
					//stop
					ver_speed = 0;
					move = false;
					calc_next_move = true;
				}
			}

			//move player on the y axis after collision checks
			y += clamp(-ver_speed, -max_speed, max_speed);
			#endregion
		}
		
		#region Detect player
		if (instance_exists(obj_player)){
			distance_from_player = distance_to_object(obj_player)
			if (distance_from_player <= max_seeing_distance){
				found = false;
				
				angle_from_player = point_direction(x, y, obj_player.x, obj_player.y);
				x_distance = lengthdir_x(distance_from_player, angle_from_player);
				y_distance = lengthdir_y(distance_from_player, angle_from_player);
				
				solid_obj = collision_line(x, y, x_distance, y_distance, obj_solid, true, true);
				
				if (solid_obj != noone){
					if (z < solid_obj.z){
						found = true;
					}
				}
				
				solid_float = collision_line(x, y, x_distance, y_distance, obj_float_solid, true, true);
				
				if (solid_float != noone){
					if(z < solid_float.z && z > solid_float.z - solid_float.height){
						found = true;
					}
				}
				
				if (!found){
					calc_next_move = true;
					move = false;
					state = Test_Enemy_State.agro;
					break;
				}
			}
		}
		#endregion
		
		#endregion
		break;
	
	case Test_Enemy_State.agro:
		break;
		
	case Test_Enemy_State.attack:
		
		
		break;
	
	case Test_Enemy_State.stun:
		
		
		
		break;
	
	case Test_Enemy_State.knockback:	
		if (knockback_first){
			zjump = true;
			zspeed = scr_vertical;
			
			hor_speed = lengthdir_x(scr_horizontal, scr_dir);	
			ver_speed = lengthdir_y(scr_horizontal, scr_dir) * -1;
			
			knockback_first = false;
		}
		
		#region movement
		
		#region Horizontal
			//right collision check
			if (hor_speed > 0){
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x + abs(hor_speed),y,obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1,y,insts)){
						x ++;
					}
		
					//stop
					hor_speed = -hor_speed/buoyancy;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x + abs(hor_speed),y,obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if ((instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height)){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1,y,instf))
					{
						x ++;
					}
		
					//stop
					hor_speed = -hor_speed/buoyancy;
				}
			}

			//left collision check
			if (hor_speed < 0){
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place((x - abs(hor_speed)),y,obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1,y,insts))
					{
						x --;
					}
		
					//stop
					hor_speed = -hor_speed/buoyancy;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x - abs(hor_speed),y,obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1,y,instf))
					{
						x --;
					}
		
					//stop
					hor_speed = -hor_speed/buoyancy;
				}
			}

			//move player on the x axis after collision checks
			x += clamp(hor_speed, -scr_horizontal, scr_horizontal);
			#endregion

			#region Vertical
			//down collision check
			if (ver_speed > 0){
	
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x,y - abs(ver_speed),obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1,insts))
					{
						y --;
					}
		
					//stop
					ver_speed = -ver_speed/buoyancy;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x,y - abs(ver_speed),obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1,instf)){
						y --;
					}
		
					//stop
					ver_speed = -ver_speed/buoyancy;
				}
			}

			// up collision check
			if (ver_speed < 0){
	
				//Grounded Solids---------------------------------------------------------------
				//reset instance for new instance
				insts = noone;
	
				//check for instance
				insts = (instance_place(x,y + abs(ver_speed),obj_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (insts != noone) and (insts.z > z){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y + 1,insts)){
						y ++;
					}
		
					//stop
					ver_speed = -ver_speed/buoyancy;
				}
	
				//Floatingh Solids---------------------------------------------------------------
				//reset instance for new instance
				instf = noone;
	
				//check for instance
				instf = (instance_place(x,y + abs(ver_speed),obj_float_solid));
	
				//if instanceexists and is taller than players z stop the player
				if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
					//ensures smooth and consistant collision
					while(!place_meeting(x, y + 1,instf)){
						y ++;
					}
		
					//stop
					ver_speed = -ver_speed/buoyancy;
				}
			}

			//move player on the y axis after collision checks
			y += clamp(-ver_speed, -scr_horizontal, scr_horizontal);
			
			#endregion
		
		#endregion
		
		break;
}

#region gravity 

if (zjump){
	inst = noone;
	inst = instance_place(x,y,obj_float_solid);
	if ((inst != noone) and (z < inst.z)){
		if (inst.z-inst.height-height > 0){
			z = clamp(z + zspeed, 0, inst.z-inst.height-height); // z pos goes up
		}
	}
	else{
		z += zspeed;
	}
}

//if not ontop of block
if ((!instance_place(x,y,obj_solid)) and (!instance_place(x,y,obj_float_solid))){
	zfloor = 0; /*zfloor is ground level*/
}

//if not on ground
if (!z <= zfloor){
	z -= zgrav; //apply downforce on z pos
	zgrav += zacce; //rav gets stronger each step
}

//stop falling when you hit zfloor
if (z <= zfloor+1/*+1 for sticking glitch on ground*/){
	zgrav = 0; //stop applying downforce
	z = zfloor;	//nap z pos to on ground
	zjump = false; //no longer in the air
	if (state == Test_Enemy_State.knockback){
		state = Test_Enemy_State.normal;
		cool_down = false;
		hor_speed = 0;
		ver_speed = 0;
	}
}

#endregion