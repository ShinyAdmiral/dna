/// @description Insert description here
if (!cool_down){
	scr_vertical = other.upward_force;
	scr_horizontal = other.forward_force;
	scr_damage = other.strength;
	scr_stun = other.stun;
	scr_dir = other.dir;

	knockback_first = true;
	state = Test_Enemy_State.knockback;
	cool_down = true;
}