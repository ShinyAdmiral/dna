{
    "id": "9ed3b866-dbf1-4897-898f-90af025b1eb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_24",
    "eventList": [
        {
            "id": "f0bcb2ed-f547-47b9-a682-fcdff5f5f209",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ed3b866-dbf1-4897-898f-90af025b1eb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5c251e7-3bc8-4efb-92ac-4ae41628009a",
    "visible": true
}