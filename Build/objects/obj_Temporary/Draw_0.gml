/// @description Temporary
draw_set_color(c_white)
draw_text(x,y-24,"'Alt + F4' key to leave | 'R' key to reset");
draw_text(x,y, "'Space for 'Slow Mode | 'P' to change Sprite Mode");
draw_text(x,y + 24,"!!!_Dependent_!!!")

draw_text(x,y + 48,"Horizontal Speed: " + string(obj_Player.hspeed))
draw_text(x,y + 72,"Vetical Speed: " +string(obj_Player.vspeed))
draw_text(x,y + 96,"Speed: " +string(obj_Player.speed))
draw_text(x,y + 120,"Direction: " +string(obj_Player.dir))
draw_text(x,y + 144+24,"!!!_Independent_!!!")
draw_text(x,y + 168+24,"Move speed: " +string(obj_Player.move_speed))
draw_text(x,y + 192+24,"Acceleration: " +string(obj_Player.acceleration))
draw_text(x,y + 216+24,"Stopping Power: " +string(obj_Player.stopping_power))
draw_text(x,y + 216+(24*3),"!!!_Mode_!!!")
draw_text(x,y + 216+(24*5),"8 Sprite Mode: " + string(dir))