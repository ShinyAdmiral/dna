{
    "id": "e1e3af2f-519b-476a-8db6-6bfa3d767295",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Temporary",
    "eventList": [
        {
            "id": "875fff3f-3c53-47f2-8fe8-9bd5462e4d44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e1e3af2f-519b-476a-8db6-6bfa3d767295"
        },
        {
            "id": "3fc47bc8-7f87-4625-b4b5-64047d7ed270",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "e1e3af2f-519b-476a-8db6-6bfa3d767295"
        },
        {
            "id": "755289f8-516e-459c-ac57-e88a37ef14c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 80,
            "eventtype": 9,
            "m_owner": "e1e3af2f-519b-476a-8db6-6bfa3d767295"
        },
        {
            "id": "8e91fb66-c40f-4860-b471-e849e5672537",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1e3af2f-519b-476a-8db6-6bfa3d767295"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}