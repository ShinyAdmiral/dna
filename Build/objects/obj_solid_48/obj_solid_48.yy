{
    "id": "1d1b9e29-4001-4bcd-82c7-9c9afe8a432d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_48",
    "eventList": [
        {
            "id": "c460f212-c456-4551-aed8-486129c11259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d1b9e29-4001-4bcd-82c7-9c9afe8a432d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e945fe29-7d73-4283-8eef-fe5aed71636f",
    "visible": true
}