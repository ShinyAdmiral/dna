/// @description For Solid Grounded Collisions
instbelow_s = instance_place(x,y,obj_solid);

if (z < zfloor){
	instance_destroy(self);
}
else{
	zfloor = instbelow_s.z;
}