/// @description For Floating Collisions
instbelow_f = instance_place(x,y,obj_float_solid);

if (z < instbelow_f.z and z > instbelow_f.z - instbelow_f.height){
	instance_destroy(self);
}

else if (z >= instbelow_f.z){
	zfloor = instbelow_f.z;
}
