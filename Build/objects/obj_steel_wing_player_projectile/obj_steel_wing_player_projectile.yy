{
    "id": "b346d16d-aa45-42ca-9ab2-da8c1cf667a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_steel_wing_player_projectile",
    "eventList": [
        {
            "id": "bfcba71d-33bd-454a-95ca-ffe91e54e824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b346d16d-aa45-42ca-9ab2-da8c1cf667a8"
        },
        {
            "id": "def412ee-b81a-478b-866b-0c22b09167f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b346d16d-aa45-42ca-9ab2-da8c1cf667a8"
        },
        {
            "id": "7cfb0831-ceae-4d5f-9624-1e5df369ba16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b346d16d-aa45-42ca-9ab2-da8c1cf667a8"
        },
        {
            "id": "1a3665a9-d075-484a-9875-7025c2b29988",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9ea9fc3-5a7c-48dd-8a2a-f7dfb9ddffd8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b346d16d-aa45-42ca-9ab2-da8c1cf667a8"
        },
        {
            "id": "5522533d-cb09-4cf8-bd11-d483c1585554",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ba96f61a-df70-4f35-935c-e4e588ae7d42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b346d16d-aa45-42ca-9ab2-da8c1cf667a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "8aabaca6-9f59-405d-864a-5cb5f509e9ce",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "dir",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "886185af-12c2-4ad8-8c03-ed372b94469d",
    "visible": true
}