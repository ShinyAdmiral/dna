{
    "id": "80447b00-a283-43c3-9bf7-5b6c51b5ee85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hitbox_test",
    "eventList": [
        {
            "id": "452c1426-d5ba-4378-8a0c-f5e997087241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80447b00-a283-43c3-9bf7-5b6c51b5ee85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "062683e5-c194-400d-a386-86524f8038b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "fa63e393-4ffb-4e62-8aea-0d449f727804",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "z",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "5976417d-f858-4acd-a71f-cf7e252689f6",
    "visible": true
}