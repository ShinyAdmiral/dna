
{
    "name": "AndrewsPlayground",
    "id": "d9c04b05-dad1-43ff-9b1e-f5d017087a12",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "0e02d207-9caa-428d-b19c-2eadbfee7e46",
        "98073bd6-8712-47e9-8ee2-d7350f91556d",
        "35dfe2bc-c35d-4f75-8ffd-62fc4e4207a6",
        "a2443ec6-fab9-4a34-bca9-85fa6220b5b5",
        "0bc594e3-70c9-4027-8bad-c09fe11f2ef3",
        "30a09b7a-d7f4-46ce-a7fe-6aa2821ea957",
        "caf27151-8631-4508-84a2-4f41e4b91422",
        "4d6e02c9-7d8c-4f73-9141-2ad41aca582e",
        "aa8f8dc8-8041-4b9f-b117-e53dcdbbeff7",
        "47c7647d-ed9a-4fd4-807f-db2c487ec693",
        "9820bab3-88a2-4cd8-b879-d3219091311a",
        "53d1807d-eb16-4ae6-9720-c0f4353760c6",
        "29891441-e26b-43ca-beeb-b7f74ea1bd6e",
        "b610e6c5-ae46-446c-b777-ec59413b08dc",
        "67e26fdd-a8d7-4991-866b-d528fb3382bf",
        "b306791b-32a0-47a7-9816-73b27af82d87",
        "49ba71a9-1f44-4db8-a4d0-a9a118bf159e",
        "be29d092-0755-429c-929d-3eb27a8d088f",
        "d94e4088-ec2d-41e2-9ff7-170804b7f51a",
        "4e53802d-eca5-4435-a0ca-92b9172e935a",
        "08b71e2e-5a44-435c-b8c9-5bf1d5dd7df4",
        "ab10cf54-777f-418b-8720-303f8edaac8e",
        "181f8f84-cc52-48e5-bc0a-feae44c4d075",
        "6a5af334-1d81-49ec-8739-305da38a1427",
        "ec564d8b-baf8-4401-9941-d4c36979fdbe",
        "464654c6-03ca-491d-b174-eb59e78e0a22",
        "c13d873d-965f-4ab1-a7f4-5f1205af8a53",
        "f185bcbd-1194-4060-852d-2cee7d35c7dc",
        "e01b7928-b570-4dc8-9886-a574ae8d6737",
        "484ead0a-1554-4c24-950d-bc864d1aadee",
        "1b75b1c6-b12b-4e2a-a691-081fcad0a139",
        "fc1b8785-4a14-4571-bb07-c4e71c2e3ca8",
        "e88a854a-732a-4b6c-a5a7-bf5782cad210",
        "3a805c47-2c8a-4c47-881e-d21242160196"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Management",
            "id": "8c9023a2-aa59-45b3-892c-4d8a0fd93fd2",
            "depth": 0,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_47F5A2C8","id": "ec564d8b-baf8-4401-9941-d4c36979fdbe","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_47F5A2C8","objId": "224fa7a1-c1b8-47fc-ba23-b13695499cd8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1148,"y": 520},
{"name": "inst_6634EE7C","id": "1b75b1c6-b12b-4e2a-a691-081fcad0a139","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6634EE7C","objId": "367b4090-77f3-4ddf-aaa8-8fbe4e556900","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 524,"y": -120},
{"name": "inst_6A10426B","id": "fc1b8785-4a14-4571-bb07-c4e71c2e3ca8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6A10426B","objId": "42b420ea-64fe-40d4-91c2-9165a4afe2c2","properties": null,"rotation": 0,"scaleX": 6,"scaleY": 4,"mvc": "1.1","x": 228,"y": -192}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRPathLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Path_1",
            "id": "19e037b0-3a80-4e01-979d-340c97d73514",
            "colour": { "Value": 4278190335 },
            "depth": 100,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRPathLayer",
            "pathId": "00000000-0000-0000-0000-000000000000",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "817f945e-8b91-409b-9064-abc011440989",
            "depth": 200,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_324E39C0","id": "d94e4088-ec2d-41e2-9ff7-170804b7f51a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_324E39C0","objId": "c2346d49-cc38-4b3d-8fc5-0c6e5bf3ba8b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 380,"y": 88},
{"name": "inst_2591D3DB","id": "4e53802d-eca5-4435-a0ca-92b9172e935a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2591D3DB","objId": "1e253367-3e46-48a1-b995-bd58d0d4d156","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 228,"y": 432},
{"name": "inst_2D88AE95","id": "3a805c47-2c8a-4c47-881e-d21242160196","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2D88AE95","objId": "32a8f2f4-7293-4a69-a0e0-d2bf1d5dc2d7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 56,"y": 76}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Ledge_Guard",
            "id": "ce0ed90d-2029-4d4d-bf53-16fd26e57d84",
            "depth": 300,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Solids",
            "id": "45ab62fe-81d7-4e0b-8c11-e495443a1e23",
            "depth": 400,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_2B0F063","id": "0e02d207-9caa-428d-b19c-2eadbfee7e46","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2B0F063","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 408},
{"name": "inst_3A58C071","id": "98073bd6-8712-47e9-8ee2-d7350f91556d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3A58C071","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 280},
{"name": "inst_644F6B71","id": "35dfe2bc-c35d-4f75-8ffd-62fc4e4207a6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_644F6B71","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 152},
{"name": "inst_1D3ED39F","id": "a2443ec6-fab9-4a34-bca9-85fa6220b5b5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1D3ED39F","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 24},
{"name": "inst_1541ACE7","id": "0bc594e3-70c9-4027-8bad-c09fe11f2ef3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1541ACE7","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 16.625,"scaleY": 1,"mvc": "1.1","x": -96,"y": -88},
{"name": "inst_77FF8BC1","id": "30a09b7a-d7f4-46ce-a7fe-6aa2821ea957","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_77FF8BC1","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 536},
{"name": "inst_5C1E09A6","id": "caf27151-8631-4508-84a2-4f41e4b91422","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5C1E09A6","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 664},
{"name": "inst_7C0AFA21","id": "4d6e02c9-7d8c-4f73-9141-2ad41aca582e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7C0AFA21","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 792},
{"name": "inst_210E451","id": "aa8f8dc8-8041-4b9f-b117-e53dcdbbeff7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_210E451","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -96,"y": 920},
{"name": "inst_4F36748D","id": "47c7647d-ed9a-4fd4-807f-db2c487ec693","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4F36748D","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 15.4375,"scaleY": 1,"mvc": "1.1","x": 32,"y": 968},
{"name": "inst_D487DA9","id": "9820bab3-88a2-4cd8-b879-d3219091311a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_D487DA9","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 408},
{"name": "inst_7B74B6BB","id": "53d1807d-eb16-4ae6-9720-c0f4353760c6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7B74B6BB","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 280},
{"name": "inst_4842A40C","id": "29891441-e26b-43ca-beeb-b7f74ea1bd6e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4842A40C","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 152},
{"name": "inst_5468E2CF","id": "b610e6c5-ae46-446c-b777-ec59413b08dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5468E2CF","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 24},
{"name": "inst_5627F2F0","id": "67e26fdd-a8d7-4991-866b-d528fb3382bf","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5627F2F0","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 536},
{"name": "inst_6F011EED","id": "b306791b-32a0-47a7-9816-73b27af82d87","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6F011EED","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 664},
{"name": "inst_7600A52A","id": "49ba71a9-1f44-4db8-a4d0-a9a118bf159e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7600A52A","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 792},
{"name": "inst_4B156095","id": "be29d092-0755-429c-929d-3eb27a8d088f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4B156095","objId": "e2dba960-1760-4e5c-b7ec-c6e2ec9a3466","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 1960,"y": 920},
{"name": "inst_26D42B","id": "08b71e2e-5a44-435c-b8c9-5bf1d5dd7df4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_26D42B","objId": "00e696fd-05c0-4155-b68f-9f9aa4cc26a6","properties": null,"rotation": 0,"scaleX": 4.625,"scaleY": 1,"mvc": "1.1","x": 752,"y": 248},
{"name": "inst_56312DD8","id": "ab10cf54-777f-418b-8720-303f8edaac8e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_56312DD8","objId": "00e696fd-05c0-4155-b68f-9f9aa4cc26a6","properties": null,"rotation": 0,"scaleX": 4.75,"scaleY": 1,"mvc": "1.1","x": 608,"y": 392},
{"name": "inst_490C628","id": "181f8f84-cc52-48e5-bc0a-feae44c4d075","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_490C628","objId": "322359ed-71fe-4e4d-944d-83d78545f0c9","properties": null,"rotation": 0,"scaleX": 6,"scaleY": 1,"mvc": "1.1","x": 360,"y": 440},
{"name": "inst_9A1EF37","id": "6a5af334-1d81-49ec-8739-305da38a1427","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_9A1EF37","objId": "322359ed-71fe-4e4d-944d-83d78545f0c9","properties": null,"rotation": 0,"scaleX": 6,"scaleY": 1,"mvc": "1.1","x": 360,"y": 472},
{"name": "inst_68FEF13F","id": "464654c6-03ca-491d-b174-eb59e78e0a22","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_68FEF13F","objId": "00e696fd-05c0-4155-b68f-9f9aa4cc26a6","properties": null,"rotation": 0,"scaleX": 2.75,"scaleY": 1,"mvc": "1.1","x": 544,"y": 248},
{"name": "inst_5C4DD7F0","id": "c13d873d-965f-4ab1-a7f4-5f1205af8a53","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5C4DD7F0","objId": "1d1b9e29-4001-4bcd-82c7-9c9afe8a432d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 496,"y": 264},
{"name": "inst_7B0ED8D7","id": "f185bcbd-1194-4060-852d-2cee7d35c7dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7B0ED8D7","objId": "322359ed-71fe-4e4d-944d-83d78545f0c9","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 464,"y": 280},
{"name": "inst_591C0499","id": "e01b7928-b570-4dc8-9886-a574ae8d6737","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_591C0499","objId": "4ee68b0c-b6ad-4df4-8669-dacb81c5e36e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 448,"y": 296},
{"name": "inst_1B65470F","id": "484ead0a-1554-4c24-950d-bc864d1aadee","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1B65470F","objId": "748ccf97-d458-422a-9648-9e2c047f70ad","properties": [{"id": "676914ee-f06f-443a-a87a-dc95ce4d141b","modelName": "GMOverriddenProperty","objectId": "748ccf97-d458-422a-9648-9e2c047f70ad","propertyId": "b23a57db-cc9a-4e9c-8fd2-b210ba79d63b","mvc": "1.0","value": "64"}],"rotation": 0,"scaleX": 1,"scaleY": 4,"mvc": "1.1","x": 604,"y": 392}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Hit_Boxes",
            "id": "fa056a12-8869-426b-b867-67478a7f0ad5",
            "depth": 500,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_5A9720B5","id": "e88a854a-732a-4b6c-a5a7-bf5782cad210","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5A9720B5","objId": "c9a9eed0-7e6d-4213-8516-57b9750e6b46","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 188,"y": 204}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "80da8fd7-6f07-496c-ab08-98fbb931ed38",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 600,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "cc8fc1cd-0fcf-4e5e-b844-cfb7de9ac2c0",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "a11b389b-ce44-449a-986b-37a6c4bbbcc3",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "7c09ddbf-d58b-44a1-aaba-3b3c55c53785",
        "Height": 1000,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 2000
    },
    "mvc": "1.0",
    "views": [
{"id": "4d515f79-4b3a-4d87-89d1-d61682b5f061","hborder": 32,"hport": 270,"hspeed": -1,"hview": 270,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 480,"wview": 480,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "258d90a7-2d3f-48c9-afc6-f32abb2ce15e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "cdc96492-f85d-4acd-9a7a-cf2658eecb68","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1bbac233-18a2-42e1-979b-10721f6c5980","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "549401c4-14cc-4691-b085-705231222d71","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ced1e44b-b822-484b-b51f-9f1f84e879f9","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "75b34845-571e-4c45-bcaa-88da96402e89","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6a223d57-e262-4efc-800e-4669b4d2be53","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "fa21ece7-cfc7-4a66-8064-80a44da1319b",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}