{
    "id": "6381d9ef-3d32-4a5a-9c3d-95c0041b2426",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_glide_dash_sweetbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 28,
    "bbox_right": 35,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6ab5d1a-b419-4c75-9052-bb5149cb3edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6381d9ef-3d32-4a5a-9c3d-95c0041b2426",
            "compositeImage": {
                "id": "cf7f2cff-95f5-45e8-8a09-bef64813b5fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ab5d1a-b419-4c75-9052-bb5149cb3edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2556788e-9edd-4702-935b-3863d01c2eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ab5d1a-b419-4c75-9052-bb5149cb3edf",
                    "LayerId": "92dfb6f5-3696-4621-8f0a-3bea1ad988be"
                },
                {
                    "id": "d5d4b8de-59e0-4e5a-82bd-c67851699806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ab5d1a-b419-4c75-9052-bb5149cb3edf",
                    "LayerId": "62127e6e-9e8f-4fe5-9793-27a87884e831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "92dfb6f5-3696-4621-8f0a-3bea1ad988be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6381d9ef-3d32-4a5a-9c3d-95c0041b2426",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "62127e6e-9e8f-4fe5-9793-27a87884e831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6381d9ef-3d32-4a5a-9c3d-95c0041b2426",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 36
}