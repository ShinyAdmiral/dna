{
    "id": "43736703-bff7-479e-b738-fa1ecd39eedb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teeter_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9978a4a9-e730-4b89-a64f-de9db8c1ab8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43736703-bff7-479e-b738-fa1ecd39eedb",
            "compositeImage": {
                "id": "e852f0f8-e945-438d-848b-ae03b5f74f45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9978a4a9-e730-4b89-a64f-de9db8c1ab8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51b99cc7-341d-4223-b2d3-72ca697da096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9978a4a9-e730-4b89-a64f-de9db8c1ab8d",
                    "LayerId": "89230ed8-10de-4b45-a2d8-31efbadb602b"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 16,
    "layers": [
        {
            "id": "89230ed8-10de-4b45-a2d8-31efbadb602b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43736703-bff7-479e-b738-fa1ecd39eedb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}