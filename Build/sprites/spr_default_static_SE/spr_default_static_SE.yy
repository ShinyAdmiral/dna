{
    "id": "fab3feea-f3c5-4cc9-a465-fcb6fe0ae530",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_static_SE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "374d04d7-7b51-48ee-b154-6ca4b256fb97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fab3feea-f3c5-4cc9-a465-fcb6fe0ae530",
            "compositeImage": {
                "id": "2e5a546b-186c-406b-b453-c06ba1a4497f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374d04d7-7b51-48ee-b154-6ca4b256fb97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51489dee-2fbc-42af-bcd5-d7d54ae16dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374d04d7-7b51-48ee-b154-6ca4b256fb97",
                    "LayerId": "8d582df9-00a2-45b8-b1f7-ae579abd1e82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8d582df9-00a2-45b8-b1f7-ae579abd1e82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fab3feea-f3c5-4cc9-a465-fcb6fe0ae530",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}