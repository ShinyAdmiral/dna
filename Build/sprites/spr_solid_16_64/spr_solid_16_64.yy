{
    "id": "54368237-d345-4c69-9c03-45d6da3c6f65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_16_64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4694b02-b32a-4359-8003-b0abd859cb22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54368237-d345-4c69-9c03-45d6da3c6f65",
            "compositeImage": {
                "id": "c9cdae2e-740b-47d7-b5d0-64b436a114df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4694b02-b32a-4359-8003-b0abd859cb22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45921aa3-e4bc-467c-bf7a-9e3d41594baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4694b02-b32a-4359-8003-b0abd859cb22",
                    "LayerId": "31daeeb8-b783-4ad3-a2d2-3247608bdc8d"
                },
                {
                    "id": "fb50d407-144c-4c4b-9d27-7548252f8078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4694b02-b32a-4359-8003-b0abd859cb22",
                    "LayerId": "f40ef1a9-2a02-424b-8422-a25e87c9d1a4"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 128,
    "layers": [
        {
            "id": "f40ef1a9-2a02-424b-8422-a25e87c9d1a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54368237-d345-4c69-9c03-45d6da3c6f65",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "31daeeb8-b783-4ad3-a2d2-3247608bdc8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54368237-d345-4c69-9c03-45d6da3c6f65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 64
}