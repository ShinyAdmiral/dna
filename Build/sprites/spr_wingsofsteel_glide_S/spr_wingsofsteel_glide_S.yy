{
    "id": "a034f722-45be-42c8-bea2-e29ec279bb41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_S",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccfe0bdb-893f-4a0e-b45a-3948aac5258e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a034f722-45be-42c8-bea2-e29ec279bb41",
            "compositeImage": {
                "id": "ec1445bb-4845-4dcc-a713-c9be2632b2a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfe0bdb-893f-4a0e-b45a-3948aac5258e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c1ecc1e-7454-4e9c-8696-18ec814dc711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfe0bdb-893f-4a0e-b45a-3948aac5258e",
                    "LayerId": "60d127f8-ce40-43a6-aa7b-eb6cb18c78d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "60d127f8-ce40-43a6-aa7b-eb6cb18c78d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a034f722-45be-42c8-bea2-e29ec279bb41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}