{
    "id": "e0d7b007-0a80-43cd-8ed7-657ba3cea0ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_N",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e4246fa-8f08-4c20-bd1c-b944f4432534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d7b007-0a80-43cd-8ed7-657ba3cea0ce",
            "compositeImage": {
                "id": "fa06a6be-1d41-4ae4-ae73-9cc3b15df7f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e4246fa-8f08-4c20-bd1c-b944f4432534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1c7ed9-741b-4147-9beb-d03ea6ef82dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e4246fa-8f08-4c20-bd1c-b944f4432534",
                    "LayerId": "eb86ccbe-debe-47bc-9946-14d54fccd1b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "eb86ccbe-debe-47bc-9946-14d54fccd1b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0d7b007-0a80-43cd-8ed7-657ba3cea0ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}