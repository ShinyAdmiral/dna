{
    "id": "e2d299b7-0077-42a6-90e9-eeb7cb5d0656",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d27186a0-68e7-4433-8267-502b0d1863c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d299b7-0077-42a6-90e9-eeb7cb5d0656",
            "compositeImage": {
                "id": "d4bb24bb-d13c-4ad3-8f2d-34b6c1766f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d27186a0-68e7-4433-8267-502b0d1863c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56e3630-7556-4171-8f77-4cf29e779026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d27186a0-68e7-4433-8267-502b0d1863c7",
                    "LayerId": "4d2680a8-aaf4-4285-a840-dc0729f1ef1b"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 128,
    "layers": [
        {
            "id": "4d2680a8-aaf4-4285-a840-dc0729f1ef1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2d299b7-0077-42a6-90e9-eeb7cb5d0656",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 64
}