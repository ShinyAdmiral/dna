{
    "id": "fcf01fe5-c481-4057-a2fc-28adf74f7a1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4febaa2f-24c0-4635-a810-b019f50b1326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcf01fe5-c481-4057-a2fc-28adf74f7a1f",
            "compositeImage": {
                "id": "5c2e8bc8-d218-4906-b8d5-3c6e77129948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4febaa2f-24c0-4635-a810-b019f50b1326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60fbc6d6-9264-492a-83f5-bf30951bc33a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4febaa2f-24c0-4635-a810-b019f50b1326",
                    "LayerId": "cd258389-59a1-4191-8d31-1c28aa15af58"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "cd258389-59a1-4191-8d31-1c28aa15af58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcf01fe5-c481-4057-a2fc-28adf74f7a1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 16
}