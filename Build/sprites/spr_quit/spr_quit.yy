{
    "id": "528b54b0-d676-4961-96f8-85caf15aa64d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_quit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddc9e84f-dbf1-4be5-ae1c-58fa3138e9de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "528b54b0-d676-4961-96f8-85caf15aa64d",
            "compositeImage": {
                "id": "30fdeee3-54a7-4dad-8248-cb027e4c1d72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddc9e84f-dbf1-4be5-ae1c-58fa3138e9de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3795679-38dd-42f5-80c2-31e0345a0d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddc9e84f-dbf1-4be5-ae1c-58fa3138e9de",
                    "LayerId": "556b62ba-ed7b-42f5-b771-c6ed6a7536f1"
                }
            ]
        },
        {
            "id": "4ba24bb8-5436-4c03-b948-6f906100255c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "528b54b0-d676-4961-96f8-85caf15aa64d",
            "compositeImage": {
                "id": "04dd38a7-a5a5-40c4-9475-cc3f42a9d5a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba24bb8-5436-4c03-b948-6f906100255c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fba1620-e872-4e9f-91cf-b7e70bb92686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba24bb8-5436-4c03-b948-6f906100255c",
                    "LayerId": "556b62ba-ed7b-42f5-b771-c6ed6a7536f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "556b62ba-ed7b-42f5-b771-c6ed6a7536f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "528b54b0-d676-4961-96f8-85caf15aa64d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 25
}