{
    "id": "a47a970e-759c-47f9-8771-239a866c20a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12961ad8-b05c-44ad-baf4-94f60c78602a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a47a970e-759c-47f9-8771-239a866c20a2",
            "compositeImage": {
                "id": "71606bff-e3d8-4cfd-bf07-028d81ca9146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12961ad8-b05c-44ad-baf4-94f60c78602a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b11f5b2-1154-4083-be09-8ec94a7406e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12961ad8-b05c-44ad-baf4-94f60c78602a",
                    "LayerId": "384e10b3-966f-4d8e-8834-908bacf46e54"
                },
                {
                    "id": "e2c2b2df-dec8-4489-bb8d-3ce895c6220a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12961ad8-b05c-44ad-baf4-94f60c78602a",
                    "LayerId": "ac23368b-17b5-4f2a-9ab1-65ecf0a83afd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac23368b-17b5-4f2a-9ab1-65ecf0a83afd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a47a970e-759c-47f9-8771-239a866c20a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "384e10b3-966f-4d8e-8834-908bacf46e54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a47a970e-759c-47f9-8771-239a866c20a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}