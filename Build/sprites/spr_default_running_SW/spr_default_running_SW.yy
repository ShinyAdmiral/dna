{
    "id": "3331c339-55c7-4205-b8ca-011d7560538d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_running_SW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f247e1e4-f7cd-4da5-90a8-105ecbef9e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "efb288ce-b9e5-409e-9a96-ae4306eee88c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f247e1e4-f7cd-4da5-90a8-105ecbef9e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c74290-49fb-4975-91e5-c4dc37214716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f247e1e4-f7cd-4da5-90a8-105ecbef9e43",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "f4b1b6aa-c5f1-4468-b8c1-795f27f5d57c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "f17c09c5-544c-4a18-92e6-d3de30642d87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b1b6aa-c5f1-4468-b8c1-795f27f5d57c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d330c0ae-2dad-4ed2-98c1-d9f320de1881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b1b6aa-c5f1-4468-b8c1-795f27f5d57c",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "57e86e5f-7b9b-4716-ac18-c64197685ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "dd9cc717-0acd-486a-b3b9-a8f107a9b4ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e86e5f-7b9b-4716-ac18-c64197685ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "924ca93a-3710-4a27-8a21-c62052975cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e86e5f-7b9b-4716-ac18-c64197685ad9",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "3544eb43-b392-49df-b6e1-99eae282a562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "3a6028a5-a7a2-4262-82d0-89a1619fd3cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3544eb43-b392-49df-b6e1-99eae282a562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4eeefde-90b6-4fef-84e5-fe4c129ecc0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3544eb43-b392-49df-b6e1-99eae282a562",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "a79f8cc0-281d-45fc-b494-526b52105858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "272c261b-d26c-498d-86e7-92005239d835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79f8cc0-281d-45fc-b494-526b52105858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3836d4e2-fd61-468b-a7d9-00601ea613e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79f8cc0-281d-45fc-b494-526b52105858",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "21c0d5dd-eab1-493b-87e0-de21f3d0c998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "e57c599a-a372-4955-9203-77679162fc7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21c0d5dd-eab1-493b-87e0-de21f3d0c998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a17efe-5e62-466c-8a6a-7fbae3cd499d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21c0d5dd-eab1-493b-87e0-de21f3d0c998",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "ad810f08-e0ae-44b8-9cdc-7c73730ad8d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "9bd87462-ae7a-400a-8b6b-260c9c3c9079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad810f08-e0ae-44b8-9cdc-7c73730ad8d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb27f001-a464-4713-b65f-e3d69da40df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad810f08-e0ae-44b8-9cdc-7c73730ad8d1",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "c16efba5-2842-432f-8d4f-9ce99d681942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "a17059f1-5baf-4633-8afa-4461d7e00bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c16efba5-2842-432f-8d4f-9ce99d681942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407f1bcb-365c-42e7-8790-a353fa2df08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16efba5-2842-432f-8d4f-9ce99d681942",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "419cb253-ae8d-4d79-a04b-74f0531037fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "2d268d34-2787-4077-87c5-6022622e8849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419cb253-ae8d-4d79-a04b-74f0531037fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5297471-1046-4c3a-9871-3d70686bc32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419cb253-ae8d-4d79-a04b-74f0531037fa",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "6dc3855a-3d88-4f20-b039-0bcd5438d90e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "33f48e64-f59d-418e-a652-ed5e6fcc7388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dc3855a-3d88-4f20-b039-0bcd5438d90e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf84c681-778c-4bd1-bc15-f78902bae0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dc3855a-3d88-4f20-b039-0bcd5438d90e",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "abb3b4ff-6fa0-4cc9-8482-5cd7fa052e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "0fa4296a-57e6-4e94-b65c-616a4a44a6e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb3b4ff-6fa0-4cc9-8482-5cd7fa052e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4dd1376-1d7a-4703-8bd4-16b7c1b8a955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb3b4ff-6fa0-4cc9-8482-5cd7fa052e44",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        },
        {
            "id": "0bca0e05-fb98-441c-b959-f4cabc259dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "compositeImage": {
                "id": "db54b0a2-e041-40f9-9058-c553f5208320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bca0e05-fb98-441c-b959-f4cabc259dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a4422cc-c91b-40a3-972f-8be555936dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bca0e05-fb98-441c-b959-f4cabc259dc0",
                    "LayerId": "d537f284-37ac-4938-9a3c-caad0bb7b86b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d537f284-37ac-4938-9a3c-caad0bb7b86b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3331c339-55c7-4205-b8ca-011d7560538d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}