{
    "id": "a05c3704-752f-4165-a69f-da753b024056",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teeter_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4de403fb-92f2-4ad7-ac75-f3c485518faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05c3704-752f-4165-a69f-da753b024056",
            "compositeImage": {
                "id": "ca30cc8f-c34f-4c2a-8436-a48d66b545db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de403fb-92f2-4ad7-ac75-f3c485518faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3aa0b5e-bbad-4a4d-a414-b6bd40c54271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de403fb-92f2-4ad7-ac75-f3c485518faa",
                    "LayerId": "3abe4fda-406f-4d1a-96e0-bc29b2664184"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 16,
    "layers": [
        {
            "id": "3abe4fda-406f-4d1a-96e0-bc29b2664184",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a05c3704-752f-4165-a69f-da753b024056",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 12
}