{
    "id": "623a87a3-12e8-4d26-aba1-d47a4a7d04c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_E",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81a67dee-c5b1-4f15-8ce9-b3b6e169da4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "623a87a3-12e8-4d26-aba1-d47a4a7d04c1",
            "compositeImage": {
                "id": "0ddbae4f-5b28-49dd-a033-399fce7a810b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a67dee-c5b1-4f15-8ce9-b3b6e169da4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "056ae19d-e617-4bc5-893c-b45042878627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a67dee-c5b1-4f15-8ce9-b3b6e169da4f",
                    "LayerId": "24728621-4727-4c42-bbd2-e07d4f91762e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "24728621-4727-4c42-bbd2-e07d4f91762e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "623a87a3-12e8-4d26-aba1-d47a4a7d04c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}