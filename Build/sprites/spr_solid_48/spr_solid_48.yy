{
    "id": "e945fe29-7d73-4283-8eef-fe5aed71636f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_48",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec198c66-4eb8-4d97-a80f-7592b5f1dbed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e945fe29-7d73-4283-8eef-fe5aed71636f",
            "compositeImage": {
                "id": "e11b0349-2a94-4119-9020-a6abe5e2fcf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec198c66-4eb8-4d97-a80f-7592b5f1dbed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5581e0e4-639a-4024-a1cc-49d843df279d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec198c66-4eb8-4d97-a80f-7592b5f1dbed",
                    "LayerId": "263a5ca9-e5b9-4d1c-8add-088b84fcea84"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 96,
    "layers": [
        {
            "id": "263a5ca9-e5b9-4d1c-8add-088b84fcea84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e945fe29-7d73-4283-8eef-fe5aed71636f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 48
}