{
    "id": "a0695f1a-5f0a-44f0-874d-3022661d3a3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floorTemp1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7035e532-0871-45b1-b5e9-f4630b549e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0695f1a-5f0a-44f0-874d-3022661d3a3b",
            "compositeImage": {
                "id": "3493f55e-94b7-4870-8a2b-b653528b0fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7035e532-0871-45b1-b5e9-f4630b549e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b6e0ac-2d10-420f-abed-3a48d8dfe11c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7035e532-0871-45b1-b5e9-f4630b549e7f",
                    "LayerId": "c43ddb58-f7e2-48ce-8c88-933eddf8fc3c"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "c43ddb58-f7e2-48ce-8c88-933eddf8fc3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0695f1a-5f0a-44f0-874d-3022661d3a3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}