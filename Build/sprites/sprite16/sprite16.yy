{
    "id": "33324bb3-6106-49c6-956c-404da60fade4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfcb7aff-9d5d-4f3e-9411-43b40233ffe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33324bb3-6106-49c6-956c-404da60fade4",
            "compositeImage": {
                "id": "5b2948e0-10c4-4e89-8f0b-ce2b89448b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfcb7aff-9d5d-4f3e-9411-43b40233ffe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8fd36fe-bafc-409d-9c4c-563e65cdb356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfcb7aff-9d5d-4f3e-9411-43b40233ffe4",
                    "LayerId": "64050f1c-5503-44f1-bb4a-f00a905aa019"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "64050f1c-5503-44f1-bb4a-f00a905aa019",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33324bb3-6106-49c6-956c-404da60fade4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}