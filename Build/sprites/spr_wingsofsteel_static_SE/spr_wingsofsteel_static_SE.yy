{
    "id": "ec5cd99f-e113-4365-b212-01735b5ab02f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_static_SE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dabf942b-515a-4bcd-a967-efd2b47d0696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec5cd99f-e113-4365-b212-01735b5ab02f",
            "compositeImage": {
                "id": "2ea351ac-882c-4bbe-9c76-1af0691e3ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabf942b-515a-4bcd-a967-efd2b47d0696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90967ac3-2277-43ef-a0a3-05dbd2528ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabf942b-515a-4bcd-a967-efd2b47d0696",
                    "LayerId": "b6713bbc-0279-42d9-98b4-d1dc8b69e4f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b6713bbc-0279-42d9-98b4-d1dc8b69e4f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec5cd99f-e113-4365-b212-01735b5ab02f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}