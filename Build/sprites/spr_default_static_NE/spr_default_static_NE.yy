{
    "id": "6f082646-8168-435d-92b0-a301ed8ac5d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_static_NE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d53f1ea3-322d-42f1-9e01-36fbe6ab8608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f082646-8168-435d-92b0-a301ed8ac5d8",
            "compositeImage": {
                "id": "79d37af7-6f62-4490-8dec-962017d2d422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d53f1ea3-322d-42f1-9e01-36fbe6ab8608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a89cec8-b226-4cf5-8d9b-438efb18a37b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d53f1ea3-322d-42f1-9e01-36fbe6ab8608",
                    "LayerId": "5f247dd9-1173-4ad4-b0e8-2fe2ef1341af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5f247dd9-1173-4ad4-b0e8-2fe2ef1341af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f082646-8168-435d-92b0-a301ed8ac5d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}