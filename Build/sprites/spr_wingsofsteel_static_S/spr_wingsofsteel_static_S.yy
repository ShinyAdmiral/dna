{
    "id": "0372df68-2e6b-4f0f-ae34-c6f77ac43f37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_static_S",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6548e60e-5906-4873-9f8f-58738787d359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0372df68-2e6b-4f0f-ae34-c6f77ac43f37",
            "compositeImage": {
                "id": "7d9b3a0d-a42c-402d-8f77-fd44550e10a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6548e60e-5906-4873-9f8f-58738787d359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0338a0e-52fe-4db9-a39f-329b36b0ed62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6548e60e-5906-4873-9f8f-58738787d359",
                    "LayerId": "e2795569-ee62-4a5c-b5ce-7479176bab94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e2795569-ee62-4a5c-b5ce-7479176bab94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0372df68-2e6b-4f0f-ae34-c6f77ac43f37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}