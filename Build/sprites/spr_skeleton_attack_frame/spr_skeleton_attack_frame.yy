{
    "id": "a7b1b3ea-0ba5-4435-bee1-6378c2ca3eae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_attack_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 11,
    "bbox_right": 51,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cb4e2d2-67c7-4cc0-937f-cb939ffcc807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b1b3ea-0ba5-4435-bee1-6378c2ca3eae",
            "compositeImage": {
                "id": "d5a09692-1d8b-45ee-98f7-74a7212def64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb4e2d2-67c7-4cc0-937f-cb939ffcc807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c26d4e-4800-49f3-9615-a9be3813ba9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb4e2d2-67c7-4cc0-937f-cb939ffcc807",
                    "LayerId": "4b2bf453-9618-45af-a8ae-4aded0344099"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4b2bf453-9618-45af-a8ae-4aded0344099",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7b1b3ea-0ba5-4435-bee1-6378c2ca3eae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 27,
    "yorig": 63
}