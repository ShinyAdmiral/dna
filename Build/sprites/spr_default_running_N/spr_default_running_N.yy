{
    "id": "a61b088c-7d88-41ad-85d0-999106240883",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_running_N",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1ee1baa-1a51-4a94-9fee-9090662368c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "5f1bb1fb-41a1-496f-b9b4-0d9defafe8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1ee1baa-1a51-4a94-9fee-9090662368c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9481dc53-6ad7-443b-9704-0d0fbd935c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1ee1baa-1a51-4a94-9fee-9090662368c1",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "b2ab466f-8820-49ce-b382-cd7e006f498a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "cc2da162-9397-4d46-b233-1f440343c8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ab466f-8820-49ce-b382-cd7e006f498a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d27746-9db4-407e-8fb2-304efdb2d755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ab466f-8820-49ce-b382-cd7e006f498a",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "9a908fb2-0540-418e-9dc8-4b6206b43089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "d74c5ee3-3476-46cc-bed7-f0ca1131fa67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a908fb2-0540-418e-9dc8-4b6206b43089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d324997a-bbfc-45cd-9380-db5d0ca2f419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a908fb2-0540-418e-9dc8-4b6206b43089",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "6ca22b77-8936-41b2-b0f3-33eb5a129f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "d1aa8d35-9035-4ec9-bb38-f63bbe4acfc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca22b77-8936-41b2-b0f3-33eb5a129f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abf463ce-4c71-45a5-82a5-c9e6e1780b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca22b77-8936-41b2-b0f3-33eb5a129f72",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "73563193-b92a-4d8d-8b9c-3dc30623d059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "be28614c-7ebf-485c-b059-daa02e043ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73563193-b92a-4d8d-8b9c-3dc30623d059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64848225-c7d0-4e5b-b648-aac730aa44c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73563193-b92a-4d8d-8b9c-3dc30623d059",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "2b778ac4-1d15-4783-916d-707088130032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "c027fc7d-68ee-4000-bd01-114135df4ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b778ac4-1d15-4783-916d-707088130032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072997a3-0111-4646-b3b6-675523ee14bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b778ac4-1d15-4783-916d-707088130032",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "1d50c8b2-efde-406d-b267-7c0eff700a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "58650588-55a1-4c31-a230-c620d6791352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d50c8b2-efde-406d-b267-7c0eff700a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c297dc6-acda-4514-9ce5-87b56c21f4a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d50c8b2-efde-406d-b267-7c0eff700a93",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "7b95ba44-f867-410f-a5d2-8350c7fe7697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "bb18e276-557b-46dc-8a0f-473c7e3f4da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b95ba44-f867-410f-a5d2-8350c7fe7697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf63b0e-de94-45e1-b8be-734acd07dcf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b95ba44-f867-410f-a5d2-8350c7fe7697",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "eed7bb13-758f-4c6b-bd04-5a3e234dbc21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "32d994ec-f883-4e00-8eb6-11279a6e8ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed7bb13-758f-4c6b-bd04-5a3e234dbc21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d167e4d-5093-4187-b3a6-b6e42361c0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed7bb13-758f-4c6b-bd04-5a3e234dbc21",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "319a8f41-f00d-49db-97ff-6de5ffa3eee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "2cb5a7bc-f905-4ae5-bfd3-cf8dd1f83164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319a8f41-f00d-49db-97ff-6de5ffa3eee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88815285-a618-466e-a3cc-37b1e5a9f7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319a8f41-f00d-49db-97ff-6de5ffa3eee9",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "dac7f989-5321-4641-baa9-91e966a15950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "81b2b651-7aee-46f2-8a8a-5fca20975e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dac7f989-5321-4641-baa9-91e966a15950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bfd66ff-0227-4f90-a3f8-9565c74ab510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dac7f989-5321-4641-baa9-91e966a15950",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        },
        {
            "id": "e4ecc520-3e26-469d-9551-d4a454ce2228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "compositeImage": {
                "id": "96f33dca-d913-4456-a340-65a1c7580dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ecc520-3e26-469d-9551-d4a454ce2228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "349f27e9-d058-4dda-87fa-cdb8adec7be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ecc520-3e26-469d-9551-d4a454ce2228",
                    "LayerId": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f52cbdea-10fc-4740-950d-aa8d4d32d9ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a61b088c-7d88-41ad-85d0-999106240883",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 45
}