{
    "id": "4d40567d-6c82-4bb5-a345-5b201fea4709",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teeter_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 12,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "400ff1c9-92a8-44b6-954c-4b83be2e7f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d40567d-6c82-4bb5-a345-5b201fea4709",
            "compositeImage": {
                "id": "1793c996-4962-4043-a149-74ad23f4b8cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "400ff1c9-92a8-44b6-954c-4b83be2e7f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed2892d-a892-45f4-866d-435929f043a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "400ff1c9-92a8-44b6-954c-4b83be2e7f5d",
                    "LayerId": "d7c92f86-2cdc-4a21-95dc-f6022ef47228"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 16,
    "layers": [
        {
            "id": "d7c92f86-2cdc-4a21-95dc-f6022ef47228",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d40567d-6c82-4bb5-a345-5b201fea4709",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 12,
    "yorig": 0
}