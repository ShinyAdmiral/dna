{
    "id": "5976417d-f858-4acd-a71f-cf7e252689f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_attack_box_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 25,
    "bbox_right": 39,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "147be9b5-bf3b-40cf-931a-f63ac4a1f346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5976417d-f858-4acd-a71f-cf7e252689f6",
            "compositeImage": {
                "id": "e351b061-3b94-45c1-8e21-670e8f096cc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147be9b5-bf3b-40cf-931a-f63ac4a1f346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db20451c-aa08-4e95-a3ba-126d817246b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147be9b5-bf3b-40cf-931a-f63ac4a1f346",
                    "LayerId": "c823cd66-cdf9-4770-a9a1-afc52c7c4072"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c823cd66-cdf9-4770-a9a1-afc52c7c4072",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5976417d-f858-4acd-a71f-cf7e252689f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 31
}