{
    "id": "4b0cff45-81c8-4244-aa60-e56da4ed9126",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_upleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdde953e-224c-4348-83a7-4c314023d608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b0cff45-81c8-4244-aa60-e56da4ed9126",
            "compositeImage": {
                "id": "5f82e940-bbcf-40a1-bfec-a65fc2e6dade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdde953e-224c-4348-83a7-4c314023d608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15269479-d037-40d2-8dbf-e5a07595e71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdde953e-224c-4348-83a7-4c314023d608",
                    "LayerId": "e1ee878e-7673-460a-bdbb-2d0e39c1df61"
                },
                {
                    "id": "d456ae2d-542b-4d54-b269-3994472d9616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdde953e-224c-4348-83a7-4c314023d608",
                    "LayerId": "2c8846f2-957b-4a71-9404-847458ed5b92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1ee878e-7673-460a-bdbb-2d0e39c1df61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b0cff45-81c8-4244-aa60-e56da4ed9126",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2c8846f2-957b-4a71-9404-847458ed5b92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b0cff45-81c8-4244-aa60-e56da4ed9126",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}