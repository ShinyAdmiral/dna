{
    "id": "7212b27d-44bb-4e27-a04d-c8de8c346607",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_downright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81b099e9-f00f-48c3-af8c-4341a2b032aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7212b27d-44bb-4e27-a04d-c8de8c346607",
            "compositeImage": {
                "id": "1c98aaba-2f4c-4237-9de6-6764590b9aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b099e9-f00f-48c3-af8c-4341a2b032aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c414d42c-d781-4354-8d98-dc82d8308173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b099e9-f00f-48c3-af8c-4341a2b032aa",
                    "LayerId": "1cc10850-1d15-4f88-a1ae-a0373cc13eb7"
                },
                {
                    "id": "b42f1cf7-d4b1-40d0-8b38-5497baf0c7ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b099e9-f00f-48c3-af8c-4341a2b032aa",
                    "LayerId": "c5d14457-bd11-4f59-9538-bea6067d8873"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1cc10850-1d15-4f88-a1ae-a0373cc13eb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7212b27d-44bb-4e27-a04d-c8de8c346607",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c5d14457-bd11-4f59-9538-bea6067d8873",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7212b27d-44bb-4e27-a04d-c8de8c346607",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}