{
    "id": "ecd83219-3687-4e43-bd89-0229c56a803a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_running_NW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de860f88-4f47-4e5c-b690-0f42acb360a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "6b651c2a-a016-409a-b3ef-cf9cc67a26e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de860f88-4f47-4e5c-b690-0f42acb360a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c736f0ae-abef-4696-990d-16f8809469eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de860f88-4f47-4e5c-b690-0f42acb360a3",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "d08ca5c5-dce5-4a6d-92e0-28415c4230ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "c5d0b639-da39-4592-bf09-5d14a249bef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08ca5c5-dce5-4a6d-92e0-28415c4230ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bbb03a-b525-4721-9f5c-3011fa087b09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08ca5c5-dce5-4a6d-92e0-28415c4230ba",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "7f3d1f87-c222-446d-90fa-b02e45235356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "bb8b86d1-e904-4469-847f-dc42057db81f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f3d1f87-c222-446d-90fa-b02e45235356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2cdb83b-5b3f-4155-aa32-5599e7324f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f3d1f87-c222-446d-90fa-b02e45235356",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "b12fd884-ee5c-4497-aa6a-149e779655e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "6084c9ab-d1b4-480c-a280-67ac5a4741e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b12fd884-ee5c-4497-aa6a-149e779655e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408cd774-9a09-4cdf-a572-8560ac32d2c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b12fd884-ee5c-4497-aa6a-149e779655e1",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "e927f327-140f-4107-b3e2-79298d3a9dcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "03390ace-000b-4e5e-94d3-155012518c91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e927f327-140f-4107-b3e2-79298d3a9dcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c27cb9-ea4f-45c4-a0bd-3092c4c51d88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e927f327-140f-4107-b3e2-79298d3a9dcb",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "e6decf87-04c3-4239-b3c1-0bb9079d96cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "de0d77da-132c-4f6b-b487-97e795502407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6decf87-04c3-4239-b3c1-0bb9079d96cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d9dea89-881f-4c31-abf5-f56754092da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6decf87-04c3-4239-b3c1-0bb9079d96cb",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "272c566a-cfd9-452b-ac59-b89ad7e2f53f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "21c51df1-9c2a-498e-90ae-7c656be47ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "272c566a-cfd9-452b-ac59-b89ad7e2f53f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5facd3f7-0a59-47d4-a88c-535041980040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "272c566a-cfd9-452b-ac59-b89ad7e2f53f",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "bb1c472c-04b4-47d3-a63a-de0756967bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "4194c113-18c8-478d-8586-ebb3ba876ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1c472c-04b4-47d3-a63a-de0756967bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c60689a-12e9-42f2-9a18-e3458688323c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1c472c-04b4-47d3-a63a-de0756967bc5",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "a06c8443-c8cc-40e9-9772-92ff924d6326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "5cc96f19-ded3-4ea5-a4fa-2a9132f07dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a06c8443-c8cc-40e9-9772-92ff924d6326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd1cd20-1e0a-4934-9fb9-23283a575047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a06c8443-c8cc-40e9-9772-92ff924d6326",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "73146481-b8d5-416d-869e-8dce3d9094b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "9322f550-bc40-4389-802b-154cf89ffb81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73146481-b8d5-416d-869e-8dce3d9094b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0145d4df-ff1c-4a2d-a24e-976d1ac408b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73146481-b8d5-416d-869e-8dce3d9094b4",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "0e1280be-4b95-4a4e-9a5c-00e2dd6c1e23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "5a83201c-cc1e-4bc4-addb-0c02f3594cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1280be-4b95-4a4e-9a5c-00e2dd6c1e23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff495a46-9e47-44dc-835e-cbd7a9ac1d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1280be-4b95-4a4e-9a5c-00e2dd6c1e23",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        },
        {
            "id": "9e0767cb-2a57-4518-b2d1-0e71b993f62c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "compositeImage": {
                "id": "cf9debcb-a3b9-40d6-b865-842529253db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0767cb-2a57-4518-b2d1-0e71b993f62c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac13309-a7da-45e5-969e-576c25600908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0767cb-2a57-4518-b2d1-0e71b993f62c",
                    "LayerId": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dbf642c2-71e3-40f2-bd7f-ae4203e81b1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecd83219-3687-4e43-bd89-0229c56a803a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}