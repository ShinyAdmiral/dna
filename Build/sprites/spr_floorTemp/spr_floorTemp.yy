{
    "id": "cc8fc1cd-0fcf-4e5e-b844-cfb7de9ac2c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floorTemp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8b462ea-8f49-4b77-a468-b52875152251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc8fc1cd-0fcf-4e5e-b844-cfb7de9ac2c0",
            "compositeImage": {
                "id": "de725a33-6106-4700-9856-66d3677be7b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b462ea-8f49-4b77-a468-b52875152251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e8bd78-2ae3-453e-8747-8ccccc1dd4bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b462ea-8f49-4b77-a468-b52875152251",
                    "LayerId": "44d11088-1719-4e0d-bde6-c4f81dac6a44"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "44d11088-1719-4e0d-bde6-c4f81dac6a44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc8fc1cd-0fcf-4e5e-b844-cfb7de9ac2c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}