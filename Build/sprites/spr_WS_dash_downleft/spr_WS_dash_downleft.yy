{
    "id": "b30ca569-f647-466f-987d-13c1c8184a09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_downleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1fa8472-9cd0-41ce-9743-a2ab6901aed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b30ca569-f647-466f-987d-13c1c8184a09",
            "compositeImage": {
                "id": "d2fe7d64-aa7d-4823-a75f-6b50eb5212bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1fa8472-9cd0-41ce-9743-a2ab6901aed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289aeea5-7e85-4ec2-9f84-bb2223f747ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1fa8472-9cd0-41ce-9743-a2ab6901aed4",
                    "LayerId": "c85613ef-b470-4e52-a4c9-89749bd52d5f"
                },
                {
                    "id": "fc5ccbcb-9fd2-42ea-bf3a-38c3a64ff996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1fa8472-9cd0-41ce-9743-a2ab6901aed4",
                    "LayerId": "af0e2076-c61a-4a1a-8ca7-73d3f16e2c08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c85613ef-b470-4e52-a4c9-89749bd52d5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b30ca569-f647-466f-987d-13c1c8184a09",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "af0e2076-c61a-4a1a-8ca7-73d3f16e2c08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b30ca569-f647-466f-987d-13c1c8184a09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}