{
    "id": "3c7d6268-74cc-4440-bd0a-d2ef26681423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_128",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 128,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8009eda-6021-42f2-92e7-e97c7137c534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c7d6268-74cc-4440-bd0a-d2ef26681423",
            "compositeImage": {
                "id": "65a89888-2efb-4964-98d4-878ee3fe0b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8009eda-6021-42f2-92e7-e97c7137c534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aee457fe-7afa-44d8-9063-b2eb4cfa3496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8009eda-6021-42f2-92e7-e97c7137c534",
                    "LayerId": "cdf52ee0-ced3-40b3-904f-f9d73a55ac43"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 256,
    "layers": [
        {
            "id": "cdf52ee0-ced3-40b3-904f-f9d73a55ac43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c7d6268-74cc-4440-bd0a-d2ef26681423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 128
}