{
    "id": "4dd1b138-39e0-420d-827c-9158eac47026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_SW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2561800-6095-4faa-b78e-f074a740af0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dd1b138-39e0-420d-827c-9158eac47026",
            "compositeImage": {
                "id": "06d17349-bf5b-40cf-997e-b378655fca21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2561800-6095-4faa-b78e-f074a740af0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89901f1-25c7-49be-ad87-eadc11475a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2561800-6095-4faa-b78e-f074a740af0e",
                    "LayerId": "253c26ac-34e7-469b-b72b-0555722bdae3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "253c26ac-34e7-469b-b72b-0555722bdae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dd1b138-39e0-420d-827c-9158eac47026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}