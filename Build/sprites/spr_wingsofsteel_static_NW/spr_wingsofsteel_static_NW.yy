{
    "id": "4799051f-916a-4af5-a4cb-c901cab81427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_static_NW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96d4442a-65c0-42e0-a975-989988aa28a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4799051f-916a-4af5-a4cb-c901cab81427",
            "compositeImage": {
                "id": "76180335-8320-4867-83e5-18bf25e1761d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d4442a-65c0-42e0-a975-989988aa28a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94754f38-f458-4dfd-a071-e1364641f426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d4442a-65c0-42e0-a975-989988aa28a5",
                    "LayerId": "716e0a37-bb3e-4db6-afbd-2957584efc14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "716e0a37-bb3e-4db6-afbd-2957584efc14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4799051f-916a-4af5-a4cb-c901cab81427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}