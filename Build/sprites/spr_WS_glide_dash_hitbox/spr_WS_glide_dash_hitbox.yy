{
    "id": "9f06fe5a-68cb-44e8-9a7d-3d3bd5c07145",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_glide_dash_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "042f287f-6ca0-459d-8273-4bbb33fbe0a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f06fe5a-68cb-44e8-9a7d-3d3bd5c07145",
            "compositeImage": {
                "id": "96f97b65-4f7c-45c6-8305-b2f2844089e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042f287f-6ca0-459d-8273-4bbb33fbe0a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "898ffb4f-cdab-4470-a3bc-7715e5ebd722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042f287f-6ca0-459d-8273-4bbb33fbe0a8",
                    "LayerId": "feaecb61-80b5-41b8-aeea-a31270fe534f"
                },
                {
                    "id": "77069889-62a6-406f-96e9-7d4b7a5b3220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042f287f-6ca0-459d-8273-4bbb33fbe0a8",
                    "LayerId": "ecce4d37-359d-405e-94f9-b31f40383830"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "feaecb61-80b5-41b8-aeea-a31270fe534f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f06fe5a-68cb-44e8-9a7d-3d3bd5c07145",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ecce4d37-359d-405e-94f9-b31f40383830",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f06fe5a-68cb-44e8-9a7d-3d3bd5c07145",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 36
}