{
    "id": "9177d533-0bc7-4188-b2d5-b80e83bdfc69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_static_SW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52e2fda3-4506-4dc2-a1b3-6dd6895c6963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9177d533-0bc7-4188-b2d5-b80e83bdfc69",
            "compositeImage": {
                "id": "a4dece51-7d66-4e72-aec9-61129b4af20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e2fda3-4506-4dc2-a1b3-6dd6895c6963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7c9664-b6c8-42e5-937f-8b4ce71e9b2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e2fda3-4506-4dc2-a1b3-6dd6895c6963",
                    "LayerId": "963024ea-a697-4860-9310-7d4b6b5c9070"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "963024ea-a697-4860-9310-7d4b6b5c9070",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9177d533-0bc7-4188-b2d5-b80e83bdfc69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}