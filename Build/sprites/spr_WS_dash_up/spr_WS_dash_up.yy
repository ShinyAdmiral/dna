{
    "id": "1b7279be-47af-42a0-be7a-e261f37c9474",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7427f71c-434d-4a9d-9efb-965101cea3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b7279be-47af-42a0-be7a-e261f37c9474",
            "compositeImage": {
                "id": "dcaaf436-d118-4904-a32b-4580680fb34d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7427f71c-434d-4a9d-9efb-965101cea3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e00646-d206-4b3f-9e82-45a5be65c489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7427f71c-434d-4a9d-9efb-965101cea3a6",
                    "LayerId": "8b4ca834-438c-46e1-8359-005dc342865e"
                },
                {
                    "id": "b3282c2d-b1cf-4d52-8926-335673379f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7427f71c-434d-4a9d-9efb-965101cea3a6",
                    "LayerId": "5c2cb226-1ea5-4e2d-81db-63fa68c6ecb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b4ca834-438c-46e1-8359-005dc342865e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b7279be-47af-42a0-be7a-e261f37c9474",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5c2cb226-1ea5-4e2d-81db-63fa68c6ecb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b7279be-47af-42a0-be7a-e261f37c9474",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}