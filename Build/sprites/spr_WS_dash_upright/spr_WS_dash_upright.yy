{
    "id": "204efdea-f3c8-4e51-9cde-270adcfc7cf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_upright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22d828a3-9c26-46db-881c-e7f5f5cd6a67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "204efdea-f3c8-4e51-9cde-270adcfc7cf4",
            "compositeImage": {
                "id": "0d20da49-fa4f-4372-bbb7-5630c47fea94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d828a3-9c26-46db-881c-e7f5f5cd6a67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e664f78c-4424-496a-be26-6c5e47f679a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d828a3-9c26-46db-881c-e7f5f5cd6a67",
                    "LayerId": "d7ef8ac3-a0ad-4683-bffe-7a211571495d"
                },
                {
                    "id": "1b3dbc3c-84c8-464a-9c32-d49101c63125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d828a3-9c26-46db-881c-e7f5f5cd6a67",
                    "LayerId": "4ab07548-77c9-49d1-9534-232df9f5e4ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d7ef8ac3-a0ad-4683-bffe-7a211571495d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "204efdea-f3c8-4e51-9cde-270adcfc7cf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4ab07548-77c9-49d1-9534-232df9f5e4ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "204efdea-f3c8-4e51-9cde-270adcfc7cf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}