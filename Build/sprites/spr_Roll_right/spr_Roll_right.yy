{
    "id": "b775bc64-2994-41df-aca3-89b8d40543f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Roll_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 30,
    "bbox_right": 49,
    "bbox_top": 55,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c33cb432-89a3-4411-8bc1-a7b7476da00e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "0ea29732-5cd0-40a5-a7a5-374f8e94f29f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33cb432-89a3-4411-8bc1-a7b7476da00e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a4bf81b-242c-4a48-b88c-37795f4dbc21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33cb432-89a3-4411-8bc1-a7b7476da00e",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "3d1991ef-f070-401c-8a4e-bc0aef8c4170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "f808c0f0-e160-492f-a0ec-8663687bbc95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1991ef-f070-401c-8a4e-bc0aef8c4170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ced65ce-d56a-41a3-8b04-9abc9bd27151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1991ef-f070-401c-8a4e-bc0aef8c4170",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "6f2c7ec6-025d-4e78-892a-cb958f17d18e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "98144bb5-70b8-4f44-a6bd-4d70581d2a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2c7ec6-025d-4e78-892a-cb958f17d18e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5765299-01e1-4c9b-8b20-294a44bb6f12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2c7ec6-025d-4e78-892a-cb958f17d18e",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "2da8df6a-1816-4acd-b3dd-918366e7455f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "14f81432-588f-4a26-a8f3-7761a5495e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da8df6a-1816-4acd-b3dd-918366e7455f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7285562-d8a0-4140-b541-0b75f8b7438f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da8df6a-1816-4acd-b3dd-918366e7455f",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "e9462172-2b88-40a5-bdba-994011a76b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "f7e238fb-6c24-4ad6-ae3f-80147f71f2df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9462172-2b88-40a5-bdba-994011a76b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f478beb-9c78-4581-a91b-8ca4861d6ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9462172-2b88-40a5-bdba-994011a76b9b",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "41ced21b-721f-4dce-b2fa-0443ac697b18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "1a4d8fa3-1bb9-404d-9c31-761b7c5acc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ced21b-721f-4dce-b2fa-0443ac697b18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbca1629-437d-4c6c-a8e0-92f9374d5be4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ced21b-721f-4dce-b2fa-0443ac697b18",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "b86db10a-ffa6-4b43-8f52-b5a6fcf84bfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "cfc7f79e-c173-419a-a25b-d4dcd3fa650c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b86db10a-ffa6-4b43-8f52-b5a6fcf84bfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4382e798-8a39-4db1-8782-8c83f5920013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b86db10a-ffa6-4b43-8f52-b5a6fcf84bfb",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "be1bd8d0-ff34-4dc8-aece-f4cfc08f9889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "4e1b4517-127e-4f1c-b3e2-4e012c9d2de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1bd8d0-ff34-4dc8-aece-f4cfc08f9889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da368260-7285-45e5-92f2-11e3392c9f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1bd8d0-ff34-4dc8-aece-f4cfc08f9889",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "aa770dec-f62a-4124-9c8c-a5d78ce27811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "a1ca9fb9-e5fc-4d25-812e-90383fee7b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa770dec-f62a-4124-9c8c-a5d78ce27811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7fb0d2-f109-44ae-965d-b2e3089795e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa770dec-f62a-4124-9c8c-a5d78ce27811",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "fc395b03-2bfb-4d65-bfb5-3f2c2888a36a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "89bf3f93-239c-4720-b8c2-a1ae81fb9837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc395b03-2bfb-4d65-bfb5-3f2c2888a36a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04f3eeb-fe18-4f49-8acc-0c90bb211575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc395b03-2bfb-4d65-bfb5-3f2c2888a36a",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        },
        {
            "id": "e6502742-0d2a-4267-9482-1fa8ddb9281b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "compositeImage": {
                "id": "aa583e43-4cca-4b28-b0b2-54b4a52c601f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6502742-0d2a-4267-9482-1fa8ddb9281b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97338418-8fa2-4b5c-91ed-75d337a1539c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6502742-0d2a-4267-9482-1fa8ddb9281b",
                    "LayerId": "067f1439-092c-4c72-b227-9a10a503f189"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "067f1439-092c-4c72-b227-9a10a503f189",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b775bc64-2994-41df-aca3-89b8d40543f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 58
}