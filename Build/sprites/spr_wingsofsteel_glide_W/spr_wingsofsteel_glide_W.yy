{
    "id": "00ad374d-525a-4a84-8534-54a6ec7731a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_W",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "965c9674-338f-466c-a4e7-7b7b6b50d2e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00ad374d-525a-4a84-8534-54a6ec7731a1",
            "compositeImage": {
                "id": "5bf36cf8-5442-4628-8343-aa763cc0d419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "965c9674-338f-466c-a4e7-7b7b6b50d2e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90393659-eb64-499f-a6b9-4ac1a292dc63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "965c9674-338f-466c-a4e7-7b7b6b50d2e1",
                    "LayerId": "4cfb7211-306a-4b8c-963c-6e1218069670"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4cfb7211-306a-4b8c-963c-6e1218069670",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00ad374d-525a-4a84-8534-54a6ec7731a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}