{
    "id": "a5da5070-4dde-48f2-9db5-8df3d24000e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_impass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 128,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d53085cb-fa1d-402c-a0be-1c7acf53e54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5da5070-4dde-48f2-9db5-8df3d24000e4",
            "compositeImage": {
                "id": "bc4b2ddc-3b24-479c-8a53-896144bbb3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d53085cb-fa1d-402c-a0be-1c7acf53e54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4869e87c-2e44-4046-856a-cc07ed277fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d53085cb-fa1d-402c-a0be-1c7acf53e54c",
                    "LayerId": "c91844de-c28a-43a9-b8dc-de0c8b4abf03"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 256,
    "layers": [
        {
            "id": "c91844de-c28a-43a9-b8dc-de0c8b4abf03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5da5070-4dde-48f2-9db5-8df3d24000e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 128
}