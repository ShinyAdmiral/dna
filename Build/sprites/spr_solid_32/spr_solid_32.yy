{
    "id": "7ba047a3-b9e2-4600-b5c8-963701c4d05c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "689c1dbc-a12b-4552-97be-65d22f0c93ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ba047a3-b9e2-4600-b5c8-963701c4d05c",
            "compositeImage": {
                "id": "9f5b4582-6d86-4312-8921-4bcfc521b821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "689c1dbc-a12b-4552-97be-65d22f0c93ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c9de5b-e2a2-4722-b8ec-a7be6b841fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "689c1dbc-a12b-4552-97be-65d22f0c93ce",
                    "LayerId": "c083e5be-dc98-4297-89da-fef4e99a743e"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "c083e5be-dc98-4297-89da-fef4e99a743e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ba047a3-b9e2-4600-b5c8-963701c4d05c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}