{
    "id": "10ac3231-5742-45f6-aa50-1095fdf402b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_SE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7918139c-cf7b-49b0-8dd8-855e3490e041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10ac3231-5742-45f6-aa50-1095fdf402b6",
            "compositeImage": {
                "id": "0ca58b1b-240f-4dc8-b714-1dd7198f951c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7918139c-cf7b-49b0-8dd8-855e3490e041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5e8ee2c-9e39-47be-a893-c6ddf087bc4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7918139c-cf7b-49b0-8dd8-855e3490e041",
                    "LayerId": "d77bc119-3abd-4960-8ce7-8f85699bdf56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d77bc119-3abd-4960-8ce7-8f85699bdf56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10ac3231-5742-45f6-aa50-1095fdf402b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}