{
    "id": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_running_SE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "388f2a7e-2673-48be-b3c3-3202911e919f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "cba3f0cc-fb53-4e60-b19b-d871f483fa8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388f2a7e-2673-48be-b3c3-3202911e919f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7efa933-ceff-42d6-9052-0ea699bbdf1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388f2a7e-2673-48be-b3c3-3202911e919f",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "9bbc8540-6c79-48b2-a2b2-f47b40c62f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "4f7d3b31-a4f0-48ab-8222-d9410af788c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bbc8540-6c79-48b2-a2b2-f47b40c62f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3477a3d6-ceb0-41e2-8172-c46d1ef20962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bbc8540-6c79-48b2-a2b2-f47b40c62f91",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "193951d1-9ecf-40cf-971a-829c40967ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "2d11b84d-06ec-481a-9e63-07b7bba36aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193951d1-9ecf-40cf-971a-829c40967ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3026d526-2280-4733-8321-75f358b12144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193951d1-9ecf-40cf-971a-829c40967ccf",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "5f6d91fa-3f10-469f-a1ac-6837ad312480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "34e93dcc-5664-474c-800b-066fcb6ee51b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6d91fa-3f10-469f-a1ac-6837ad312480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960041f9-22fa-4dcb-ab13-4e38b8d2d748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6d91fa-3f10-469f-a1ac-6837ad312480",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "d5785127-6ae8-4eb5-ab15-6099ad5a1687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "623dcce1-5547-45ac-9c8f-08d5a0d41403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5785127-6ae8-4eb5-ab15-6099ad5a1687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f1d888-fa7b-4f8c-93af-b53be2d91e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5785127-6ae8-4eb5-ab15-6099ad5a1687",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "b0e69450-c71e-4986-8d2d-40d6ba30f622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "232fbc39-a9e4-4ccc-be17-bf6b9ca1f87d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0e69450-c71e-4986-8d2d-40d6ba30f622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea4060c-0d04-451e-995d-aaafc565d1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0e69450-c71e-4986-8d2d-40d6ba30f622",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "2d0cdb1e-a121-40e1-8b17-66c99889690b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "420cb74b-4d68-4a09-83d5-b22a44bd5994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d0cdb1e-a121-40e1-8b17-66c99889690b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5c7387-3d58-44dc-87eb-7ba860b862d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0cdb1e-a121-40e1-8b17-66c99889690b",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "13c33e5d-e8c1-4881-9843-200eb92b76f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "ec63834e-5695-4163-b48e-3fdd5eb669b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c33e5d-e8c1-4881-9843-200eb92b76f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b618f55-4d40-4a52-9511-cee783484d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c33e5d-e8c1-4881-9843-200eb92b76f2",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "6e0eba81-beb5-4d94-8a81-e8b31d90f29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "c502faf4-a8f8-4dfb-830c-136b06322303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0eba81-beb5-4d94-8a81-e8b31d90f29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb2d503-6937-4f84-a484-1164680862c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0eba81-beb5-4d94-8a81-e8b31d90f29d",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "3bce5c6a-94d0-48c5-b571-1558e2a827eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "a36a1092-efdd-4ec0-a1da-0e25f2861afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bce5c6a-94d0-48c5-b571-1558e2a827eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f188558-aa5e-498e-adbe-3ebb79a2d432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bce5c6a-94d0-48c5-b571-1558e2a827eb",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "b2df0f32-f698-4498-bd33-cd9f87b4c822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "fe5827c8-140a-43c5-a6dc-103ee33e733d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2df0f32-f698-4498-bd33-cd9f87b4c822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32a4faf7-ec2d-4475-98f5-519d59a66678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2df0f32-f698-4498-bd33-cd9f87b4c822",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        },
        {
            "id": "7043eadb-0267-42fb-964b-f2d5dee17070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "compositeImage": {
                "id": "82c61f8a-b3a6-4607-aadb-d2eead3bd044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7043eadb-0267-42fb-964b-f2d5dee17070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0490d64-ef8d-4ea9-8c27-9a5edcd41d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7043eadb-0267-42fb-964b-f2d5dee17070",
                    "LayerId": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2fb0ae0c-b8ff-44eb-b0e0-9f5161fe5246",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea751d4b-74c1-40a1-bd1d-abe832eb6e65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}