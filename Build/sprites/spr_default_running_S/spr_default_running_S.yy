{
    "id": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_running_S",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a603d925-4afb-4c9a-a025-4704b945938c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "4aa295bb-6627-4592-9c69-868b987bb926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a603d925-4afb-4c9a-a025-4704b945938c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "020367d7-d093-4ac9-8bad-5654cec846be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a603d925-4afb-4c9a-a025-4704b945938c",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "1e2f245a-1fec-4cdb-a4bb-fa567d7018f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "cc28edb4-0967-457b-84f4-58d6ca7dab99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2f245a-1fec-4cdb-a4bb-fa567d7018f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc0f35a-d9ce-4786-9fc0-4865adb3c688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2f245a-1fec-4cdb-a4bb-fa567d7018f1",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "0b571c21-069a-483b-9ae6-7829be3fa941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "a566d797-f7d4-45c8-a06f-5b77845525a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b571c21-069a-483b-9ae6-7829be3fa941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ce2974d-551d-45a1-9390-ac8e66404c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b571c21-069a-483b-9ae6-7829be3fa941",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "be60bf13-a920-434c-95b8-b0018f7b7668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "bf3be6fa-e660-4487-ba5f-54fe80cae342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be60bf13-a920-434c-95b8-b0018f7b7668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a65005-e502-46ac-9228-462dbd5f640c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be60bf13-a920-434c-95b8-b0018f7b7668",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "ce3dd3b6-e87b-4cff-9236-8767f1d70378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "54416d5b-6310-43b7-9216-896d6ea8d175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce3dd3b6-e87b-4cff-9236-8767f1d70378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136f230b-90ec-450a-968e-d868964cfee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3dd3b6-e87b-4cff-9236-8767f1d70378",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "f1ccd4b2-93d3-4ea3-89af-6ad969d7006a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "debd3136-4bdd-4b08-86a5-4dcbe7f0dd8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ccd4b2-93d3-4ea3-89af-6ad969d7006a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18aa9bb6-f285-413a-bafb-a7398249d435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ccd4b2-93d3-4ea3-89af-6ad969d7006a",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "5fe81b8d-ce5f-4adc-8cce-087cafc330f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "8f206a48-6c40-4855-add7-02c7dbcf23b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe81b8d-ce5f-4adc-8cce-087cafc330f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faeb5fb5-b607-429c-98dd-b75209247cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe81b8d-ce5f-4adc-8cce-087cafc330f7",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "bf82b049-3fb7-4c73-aef9-4137df1763ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "6b55f0b9-7a92-4972-9e3a-2f8862c4309f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf82b049-3fb7-4c73-aef9-4137df1763ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f569dfbc-673a-4387-a9d1-7cdb043bd0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf82b049-3fb7-4c73-aef9-4137df1763ae",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "192e3be3-ba69-498e-849e-be8b1ee562d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "bb19ffb8-614b-4fd0-a456-e9319b39954b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "192e3be3-ba69-498e-849e-be8b1ee562d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b05074-cf35-4dde-8a39-35e8dc7db808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "192e3be3-ba69-498e-849e-be8b1ee562d7",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "23b124c6-4469-4d4e-af21-1ac8182a64a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "27330a28-8f39-4418-a4a7-c212d450a187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b124c6-4469-4d4e-af21-1ac8182a64a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9659234f-1948-4da0-bf6b-2d8c5fe44568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b124c6-4469-4d4e-af21-1ac8182a64a3",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "46d281f6-2e53-47b2-948e-0330f1452e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "f5cc1350-0854-4fe4-9383-994419e57864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d281f6-2e53-47b2-948e-0330f1452e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e7c4b9-d1db-4c5a-9fe2-aa8cd2a6c445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d281f6-2e53-47b2-948e-0330f1452e66",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        },
        {
            "id": "cff684f0-b57c-44f5-8267-41705ebe469b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "compositeImage": {
                "id": "59da4081-bea4-420c-8d59-689e305d42b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cff684f0-b57c-44f5-8267-41705ebe469b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0de9283-f3a8-49a5-a70e-9b043608fc61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cff684f0-b57c-44f5-8267-41705ebe469b",
                    "LayerId": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0080d0b9-62fc-4d4a-a3fd-b408a21d0200",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e7246d7-9788-431a-ad2d-4ec765e369f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}