{
    "id": "9a006e31-6bd3-40b0-bfe5-12afed0d3cd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menuCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "061d6a42-b060-4b57-b678-ad0af1e9fd91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a006e31-6bd3-40b0-bfe5-12afed0d3cd1",
            "compositeImage": {
                "id": "ca2587bd-b8f4-4912-b692-581db3e0a604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061d6a42-b060-4b57-b678-ad0af1e9fd91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c840b803-a4b7-48c1-adac-ffdf664e9f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061d6a42-b060-4b57-b678-ad0af1e9fd91",
                    "LayerId": "f1a6f545-d960-40b6-b357-1b0f20f3dfaf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1a6f545-d960-40b6-b357-1b0f20f3dfaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a006e31-6bd3-40b0-bfe5-12afed0d3cd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 3
}