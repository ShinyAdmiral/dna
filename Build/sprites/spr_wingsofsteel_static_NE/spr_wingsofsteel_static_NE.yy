{
    "id": "0b1c9009-ace7-4571-b3ea-c25481937fe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_static_NE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "476de2df-1ad8-4e75-83d6-3a8b7d51bd44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b1c9009-ace7-4571-b3ea-c25481937fe0",
            "compositeImage": {
                "id": "e8523b1e-f3d5-4884-a385-20b955f066c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "476de2df-1ad8-4e75-83d6-3a8b7d51bd44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1d55f1-de8c-4118-93bd-16d790e94ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "476de2df-1ad8-4e75-83d6-3a8b7d51bd44",
                    "LayerId": "f6c9b0b4-487c-410f-83c0-a30eaf27c713"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f6c9b0b4-487c-410f-83c0-a30eaf27c713",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b1c9009-ace7-4571-b3ea-c25481937fe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}