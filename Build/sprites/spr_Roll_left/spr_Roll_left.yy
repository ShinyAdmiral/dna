{
    "id": "518260ea-efc8-494e-b436-6603092d13dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Roll_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 30,
    "bbox_right": 49,
    "bbox_top": 55,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5675bb1e-4efa-4e97-bb1e-1d0ad8d6cdb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "62c7c0ba-d681-442a-8fc9-c819888e9245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5675bb1e-4efa-4e97-bb1e-1d0ad8d6cdb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eba3f1cf-8432-45be-95ee-a605349e4ec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5675bb1e-4efa-4e97-bb1e-1d0ad8d6cdb1",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "a64bec03-9d1d-4f13-8623-a65eea56e0a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "85386577-da9f-4632-ad86-097853eef45e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a64bec03-9d1d-4f13-8623-a65eea56e0a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47b36a5-05f1-4909-b8e8-3ccefe2326c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a64bec03-9d1d-4f13-8623-a65eea56e0a1",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "2b20faa3-2222-4ca0-b3f6-0f23fc86698f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "3b624979-3893-4b11-9a87-48e6dbf83d36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b20faa3-2222-4ca0-b3f6-0f23fc86698f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88962cf8-3c76-4447-ad4f-e360cc63d446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b20faa3-2222-4ca0-b3f6-0f23fc86698f",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "e13aac3e-00c8-4679-9109-a78c510547a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "bfc65c72-6851-4d16-a9bd-e2dbc59edc71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e13aac3e-00c8-4679-9109-a78c510547a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff14e55-d8b8-4db5-8a3c-2e46a18944f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e13aac3e-00c8-4679-9109-a78c510547a0",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "fc0637ca-9687-467e-94a0-e8b3a13d0fdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "d30007f9-bdd9-4885-94a1-c965e44718c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0637ca-9687-467e-94a0-e8b3a13d0fdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692b74d3-8501-476c-be11-fd5037f3daa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0637ca-9687-467e-94a0-e8b3a13d0fdf",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "35600d53-1cb3-4350-b3c7-e72d4ca57fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "4ab3a673-d0ce-47e9-b4d3-22d2cfa603e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35600d53-1cb3-4350-b3c7-e72d4ca57fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0b5f06-e075-4e93-9a79-96165a125c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35600d53-1cb3-4350-b3c7-e72d4ca57fcc",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "7901327b-2fdd-4cca-a419-4a834d2572a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "47a59094-11fc-4693-b1cd-124428ac9445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7901327b-2fdd-4cca-a419-4a834d2572a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "745dcef2-af4c-46af-bac0-3702f0eab316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7901327b-2fdd-4cca-a419-4a834d2572a2",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "e2c6e05c-8425-49e9-82bb-d64d45fe50bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "61d5a7c9-3af2-478d-9b1a-a4dd8ba02cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2c6e05c-8425-49e9-82bb-d64d45fe50bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d12453-dcbd-4bd8-9a4e-b3ca747b4ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2c6e05c-8425-49e9-82bb-d64d45fe50bb",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "be5d45ac-a48d-4e52-87cf-b561d1a06026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "7b7c5eb8-a9c6-474f-8e3a-73e840843308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5d45ac-a48d-4e52-87cf-b561d1a06026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49388ca3-f5b9-4bcb-9cfd-40485916685c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5d45ac-a48d-4e52-87cf-b561d1a06026",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "95622ace-c6f2-4577-8fb8-49c07af3ab8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "5a04f9b5-43f6-4c9d-8114-4cfe5d41460b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95622ace-c6f2-4577-8fb8-49c07af3ab8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f6bd93-4db7-4f38-98ee-ed01c070e9bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95622ace-c6f2-4577-8fb8-49c07af3ab8d",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        },
        {
            "id": "0a3bb83b-95b5-4cb7-a9a3-a1882a99767a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "compositeImage": {
                "id": "230d4c23-8fd0-4993-b032-34cc0e9901ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a3bb83b-95b5-4cb7-a9a3-a1882a99767a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fef1e4d8-6aa5-40a0-b0d3-198bb1d1c0fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a3bb83b-95b5-4cb7-a9a3-a1882a99767a",
                    "LayerId": "721580ca-433f-4735-8cee-4219a65de498"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "721580ca-433f-4735-8cee-4219a65de498",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "518260ea-efc8-494e-b436-6603092d13dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 58
}