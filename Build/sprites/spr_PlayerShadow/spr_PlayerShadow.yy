{
    "id": "b4f62e87-d9db-4716-8bbe-b105efad6f19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51a05e28-6fe4-4672-869b-0ebfafcef431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4f62e87-d9db-4716-8bbe-b105efad6f19",
            "compositeImage": {
                "id": "3baf7b46-7cf3-4158-9cc9-0004cb038421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a05e28-6fe4-4672-869b-0ebfafcef431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a6a5c8-2e34-4496-b7f2-f66d480deca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a05e28-6fe4-4672-869b-0ebfafcef431",
                    "LayerId": "c0f1473a-79b7-44bd-a532-fe5958f7b090"
                },
                {
                    "id": "56fb24d0-4da3-438f-aced-45a2dd5e5345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a05e28-6fe4-4672-869b-0ebfafcef431",
                    "LayerId": "e6e0911a-9a28-480e-afb4-d4d67a7c8c3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c0f1473a-79b7-44bd-a532-fe5958f7b090",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4f62e87-d9db-4716-8bbe-b105efad6f19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "e6e0911a-9a28-480e-afb4-d4d67a7c8c3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4f62e87-d9db-4716-8bbe-b105efad6f19",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 35
}