{
    "id": "a5c251e7-3bc8-4efb-92ac-4ae41628009a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6448f7c7-0bd1-427b-b041-4375f9165abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5c251e7-3bc8-4efb-92ac-4ae41628009a",
            "compositeImage": {
                "id": "092d845f-242e-4b8b-b5ab-d31b5eec104a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6448f7c7-0bd1-427b-b041-4375f9165abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a1bf2d5-fe43-4033-af99-d5256f6c6888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6448f7c7-0bd1-427b-b041-4375f9165abc",
                    "LayerId": "ef2145f2-b9d0-4114-ab68-98bcdf3d72bb"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 48,
    "layers": [
        {
            "id": "ef2145f2-b9d0-4114-ab68-98bcdf3d72bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5c251e7-3bc8-4efb-92ac-4ae41628009a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 24
}