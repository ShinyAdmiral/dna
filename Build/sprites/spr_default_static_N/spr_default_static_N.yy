{
    "id": "30acfc85-82bc-414f-a1fe-2812c6776d56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_static_N",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61fe7a00-68ca-46d9-8465-1d919c596540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30acfc85-82bc-414f-a1fe-2812c6776d56",
            "compositeImage": {
                "id": "2daf9933-5d69-4eb5-86a5-31fd399177c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61fe7a00-68ca-46d9-8465-1d919c596540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55672f0a-97e2-4fe3-9e7e-2bf73d7cea82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61fe7a00-68ca-46d9-8465-1d919c596540",
                    "LayerId": "7b5fa150-999e-4db6-9683-3860b5a5c1fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7b5fa150-999e-4db6-9683-3860b5a5c1fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30acfc85-82bc-414f-a1fe-2812c6776d56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 45
}