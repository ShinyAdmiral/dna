{
    "id": "fd26dae8-3a81-48df-a5c7-3c51e050f37f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_static_SW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "794aadc3-e7c5-4334-810a-f5a5ea0647c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd26dae8-3a81-48df-a5c7-3c51e050f37f",
            "compositeImage": {
                "id": "f415ae28-29ac-45d1-91c6-be7f48a16707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "794aadc3-e7c5-4334-810a-f5a5ea0647c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7308c1-db08-4143-9e24-1d64ef77bc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "794aadc3-e7c5-4334-810a-f5a5ea0647c9",
                    "LayerId": "73943589-fb9f-4e21-be61-12505a11e33d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "73943589-fb9f-4e21-be61-12505a11e33d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd26dae8-3a81-48df-a5c7-3c51e050f37f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}