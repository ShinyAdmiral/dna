{
    "id": "f0bc753c-b859-4112-be9a-59c49f329c6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_NW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d55d3a4-f377-4c5f-a673-f14eefe9d6e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0bc753c-b859-4112-be9a-59c49f329c6a",
            "compositeImage": {
                "id": "32485f6c-2eee-4f18-b3a3-193c735d0038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d55d3a4-f377-4c5f-a673-f14eefe9d6e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f05cf03-67cf-43cf-8bd5-9011887ef4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d55d3a4-f377-4c5f-a673-f14eefe9d6e2",
                    "LayerId": "6625ac3e-d067-48de-bdf6-6c4eaa136091"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6625ac3e-d067-48de-bdf6-6c4eaa136091",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0bc753c-b859-4112-be9a-59c49f329c6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}