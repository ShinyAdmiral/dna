{
    "id": "f6e9abba-c756-4b63-9414-8935ee1ecca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_static_NW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a60ce734-958f-4d41-a646-031ae05783a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6e9abba-c756-4b63-9414-8935ee1ecca3",
            "compositeImage": {
                "id": "a28456ba-b843-46db-8f31-dbe27640ad76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a60ce734-958f-4d41-a646-031ae05783a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1c80fb-d2ea-4abd-af94-eff555b2d12c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a60ce734-958f-4d41-a646-031ae05783a2",
                    "LayerId": "073d7b32-e3e4-4662-8010-aaa782ee7112"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "073d7b32-e3e4-4662-8010-aaa782ee7112",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6e9abba-c756-4b63-9414-8935ee1ecca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}