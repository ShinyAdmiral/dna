{
    "id": "3d73a815-0281-4c48-9fca-9ce142592fb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_running_NE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7d84316-199f-4981-814a-5796de1be318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "be6100fc-8641-4b99-a0f8-db612008aaf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d84316-199f-4981-814a-5796de1be318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "128e90c4-6d92-437f-8c1d-660a4a002877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d84316-199f-4981-814a-5796de1be318",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "4a12e215-bd27-484b-89fa-2765eaebe2d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "7228ebc8-243c-488b-a506-1760b83f044e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a12e215-bd27-484b-89fa-2765eaebe2d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c15a004-7706-403d-838e-13d7c97bffc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a12e215-bd27-484b-89fa-2765eaebe2d4",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "03887a92-1c3c-423d-8e13-a3c9a4544e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "8a8c5c6c-a5a0-4d01-8448-663db32ff75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03887a92-1c3c-423d-8e13-a3c9a4544e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf2c75e-49f1-468c-ab3e-fdc1bfc5f541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03887a92-1c3c-423d-8e13-a3c9a4544e5c",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "dc5f10cc-e137-4e02-9cc4-9ee73fa739e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "c8e1897c-4749-49a0-bef5-74732fddcb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5f10cc-e137-4e02-9cc4-9ee73fa739e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f47505f-dba5-4364-8ad2-5100d47d18aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5f10cc-e137-4e02-9cc4-9ee73fa739e6",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "8c71493e-3cce-4ae7-9082-30510ff21c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "56add142-c8bc-4282-bfe7-779eab1dc778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c71493e-3cce-4ae7-9082-30510ff21c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf937db7-308c-45f6-bc99-292df7429a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c71493e-3cce-4ae7-9082-30510ff21c5b",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "c5af3026-e57b-4fb8-aa0f-bfb79512a743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "88ebecdd-77a2-4560-9a69-5ed0441777d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5af3026-e57b-4fb8-aa0f-bfb79512a743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54a9b8c4-99f6-406b-bda1-02fd815af526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5af3026-e57b-4fb8-aa0f-bfb79512a743",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "514e47df-5d92-4acf-84da-2e68ec4832ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "3b404e8e-7ce6-4814-9201-77b0b1bd6e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514e47df-5d92-4acf-84da-2e68ec4832ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a54811-8bad-4c34-b076-702ef071126e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514e47df-5d92-4acf-84da-2e68ec4832ab",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "1304ef77-aefa-4e91-8fca-ae587e182c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "0ffba9ed-0522-44bb-abc4-5fa410125416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1304ef77-aefa-4e91-8fca-ae587e182c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd1361bc-b58a-484c-a1a9-41ebf9312bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1304ef77-aefa-4e91-8fca-ae587e182c56",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "0cdd74af-2215-4063-8ba6-5d67f8ca199e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "c62104ab-b078-4662-95f8-44d95e5b66a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cdd74af-2215-4063-8ba6-5d67f8ca199e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6f9e50-e4e7-4898-9293-fd47a51e4e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cdd74af-2215-4063-8ba6-5d67f8ca199e",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "92f3c74f-9550-4178-9092-bb4303f5bebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "b86208f4-bd7f-4555-8aea-d3dcd3770411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92f3c74f-9550-4178-9092-bb4303f5bebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445dfcaa-88aa-43af-97c6-27118c56a307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92f3c74f-9550-4178-9092-bb4303f5bebe",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "83bc9781-a5f5-40e4-b47d-f6c539e03c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "f0f23477-7410-4ea8-9e36-197d8e10617e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83bc9781-a5f5-40e4-b47d-f6c539e03c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3f8444e-490e-4bca-8442-8775f0b2df7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83bc9781-a5f5-40e4-b47d-f6c539e03c5c",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        },
        {
            "id": "6f5b841d-ef0a-4a4d-9f8a-52eba277f8b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "compositeImage": {
                "id": "192a7b73-f486-4959-9581-16328e647cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f5b841d-ef0a-4a4d-9f8a-52eba277f8b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ec28b2-1886-4f62-92eb-1f439fac6fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f5b841d-ef0a-4a4d-9f8a-52eba277f8b9",
                    "LayerId": "1646440d-8093-4957-a0fd-5699550d58a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1646440d-8093-4957-a0fd-5699550d58a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d73a815-0281-4c48-9fca-9ce142592fb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}