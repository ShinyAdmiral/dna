{
    "id": "a12518ce-8d40-42f5-8236-06db65af6b94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18f8c5c2-5e12-4324-ac46-06bab264ce25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a12518ce-8d40-42f5-8236-06db65af6b94",
            "compositeImage": {
                "id": "050dbb15-4807-45e0-8e96-c1a0a15b5a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f8c5c2-5e12-4324-ac46-06bab264ce25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48298bd-d097-42fb-b0af-46737b58c347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f8c5c2-5e12-4324-ac46-06bab264ce25",
                    "LayerId": "7d6dee2b-6595-4e39-94d4-2ffdae0f9019"
                },
                {
                    "id": "9613a94e-fd4b-438a-8935-3ee73d1eb051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f8c5c2-5e12-4324-ac46-06bab264ce25",
                    "LayerId": "8d4e5a6d-5a49-44db-baef-7576db8875fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7d6dee2b-6595-4e39-94d4-2ffdae0f9019",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a12518ce-8d40-42f5-8236-06db65af6b94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8d4e5a6d-5a49-44db-baef-7576db8875fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a12518ce-8d40-42f5-8236-06db65af6b94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}