{
    "id": "2dba1b5a-a098-496f-9bb2-dec864d64980",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_static_N",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "286a3211-bcd0-46b8-9177-a5ffa7fd3d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dba1b5a-a098-496f-9bb2-dec864d64980",
            "compositeImage": {
                "id": "3c6b9431-5351-4dc3-9752-44d786c6544e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "286a3211-bcd0-46b8-9177-a5ffa7fd3d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84d11e1-3bd6-403b-bf88-b4cbe925d7db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "286a3211-bcd0-46b8-9177-a5ffa7fd3d0c",
                    "LayerId": "d396e4a3-d8bf-4942-a0d8-874698f09ad6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d396e4a3-d8bf-4942-a0d8-874698f09ad6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dba1b5a-a098-496f-9bb2-dec864d64980",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}