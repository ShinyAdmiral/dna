{
    "id": "abcd6ec1-fd4c-4e61-8a28-68d89f6a9ee6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teeter_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99ab3b97-4529-4edc-a52f-822eec52b47a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abcd6ec1-fd4c-4e61-8a28-68d89f6a9ee6",
            "compositeImage": {
                "id": "49d539d2-1ab0-4333-8ffc-c802a26993de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ab3b97-4529-4edc-a52f-822eec52b47a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d3e007-7ff1-4f98-8f08-8c477468925c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ab3b97-4529-4edc-a52f-822eec52b47a",
                    "LayerId": "cc18abbd-b6b9-4411-897b-731da1b9dc09"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 16,
    "layers": [
        {
            "id": "cc18abbd-b6b9-4411-897b-731da1b9dc09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abcd6ec1-fd4c-4e61-8a28-68d89f6a9ee6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}