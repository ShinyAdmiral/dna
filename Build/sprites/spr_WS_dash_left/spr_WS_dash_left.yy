{
    "id": "b5e44012-f428-4ef7-8e18-8f99234d2a91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_dash_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71511837-9980-4979-b69e-c021b51939d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5e44012-f428-4ef7-8e18-8f99234d2a91",
            "compositeImage": {
                "id": "8804422a-6422-4879-9206-139208439a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71511837-9980-4979-b69e-c021b51939d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66130e42-e974-444a-9c9a-cda7c6a061f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71511837-9980-4979-b69e-c021b51939d9",
                    "LayerId": "fb016027-fea3-4f59-b470-2c8370688f8d"
                },
                {
                    "id": "c6ea56bf-6f24-468a-b165-a89741d8957c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71511837-9980-4979-b69e-c021b51939d9",
                    "LayerId": "5a11d373-b054-4ff1-abcb-6df1a99410b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb016027-fea3-4f59-b470-2c8370688f8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5e44012-f428-4ef7-8e18-8f99234d2a91",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5a11d373-b054-4ff1-abcb-6df1a99410b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5e44012-f428-4ef7-8e18-8f99234d2a91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}