{
    "id": "aebe0d84-4488-4c6e-9155-210355f0ebff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_16_80",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 80,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eb311f9-85ca-427a-beee-9f6f7b476cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aebe0d84-4488-4c6e-9155-210355f0ebff",
            "compositeImage": {
                "id": "a31bedaa-76e6-4ec3-be01-42876eb14bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eb311f9-85ca-427a-beee-9f6f7b476cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b85b84-76f9-4287-a065-c7c72db19330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eb311f9-85ca-427a-beee-9f6f7b476cfa",
                    "LayerId": "db7bc846-d39e-4460-9cd7-e33cc3b78ce0"
                },
                {
                    "id": "b9a61887-11dc-4ab0-adc6-73177cbd2d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eb311f9-85ca-427a-beee-9f6f7b476cfa",
                    "LayerId": "0ce31e1e-86bf-48ee-a6fa-de0232bb65c9"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 160,
    "layers": [
        {
            "id": "db7bc846-d39e-4460-9cd7-e33cc3b78ce0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aebe0d84-4488-4c6e-9155-210355f0ebff",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0ce31e1e-86bf-48ee-a6fa-de0232bb65c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aebe0d84-4488-4c6e-9155-210355f0ebff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 80
}