{
    "id": "d5bca095-4c65-41b8-be7e-a4ec6268a1d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_continue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18caada4-1550-42aa-b654-884bc6dff938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5bca095-4c65-41b8-be7e-a4ec6268a1d2",
            "compositeImage": {
                "id": "8003ca6e-c0c6-4c21-a2d4-6b0cb763ae1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18caada4-1550-42aa-b654-884bc6dff938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9f0373-501e-4dda-aaa2-4e32cf57946e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18caada4-1550-42aa-b654-884bc6dff938",
                    "LayerId": "75700804-1656-4c17-999a-e12dd5639936"
                }
            ]
        },
        {
            "id": "3262bf8a-98d9-46bf-b320-7fd555534b18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5bca095-4c65-41b8-be7e-a4ec6268a1d2",
            "compositeImage": {
                "id": "f8856e80-603d-4759-ba28-ecb495aed472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3262bf8a-98d9-46bf-b320-7fd555534b18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4b9220d-cf53-421f-8b4a-0825b3b7f6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3262bf8a-98d9-46bf-b320-7fd555534b18",
                    "LayerId": "75700804-1656-4c17-999a-e12dd5639936"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "75700804-1656-4c17-999a-e12dd5639936",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5bca095-4c65-41b8-be7e-a4ec6268a1d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 25
}