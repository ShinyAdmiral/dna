{
    "id": "886185af-12c2-4ad8-8c03-ed372b94469d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite27",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70de161a-de28-44dd-91c7-d04d1bae4c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "886185af-12c2-4ad8-8c03-ed372b94469d",
            "compositeImage": {
                "id": "7d11df9c-2302-43b5-af98-1c4ce8638618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70de161a-de28-44dd-91c7-d04d1bae4c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9ef642-fb7b-426c-b2ae-cc21f95bf19d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70de161a-de28-44dd-91c7-d04d1bae4c96",
                    "LayerId": "e1b080cc-0675-4367-8063-ca745e2a88af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "e1b080cc-0675-4367-8063-ca745e2a88af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "886185af-12c2-4ad8-8c03-ed372b94469d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 11,
    "yorig": 6
}