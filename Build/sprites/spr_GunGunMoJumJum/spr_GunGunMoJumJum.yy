{
    "id": "a41cb783-87e9-4ee4-9565-b2c3d67c3e7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GunGunMoJumJum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6e627aa-c0b2-4504-8e8c-8e06f9048c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a41cb783-87e9-4ee4-9565-b2c3d67c3e7e",
            "compositeImage": {
                "id": "16dec5d3-9227-47a7-bdb0-039c9f07a1e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e627aa-c0b2-4504-8e8c-8e06f9048c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b819192e-6b13-4703-b364-e628314216e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e627aa-c0b2-4504-8e8c-8e06f9048c73",
                    "LayerId": "0d8fee62-3305-4963-ab0d-a85bef877622"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "0d8fee62-3305-4963-ab0d-a85bef877622",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a41cb783-87e9-4ee4-9565-b2c3d67c3e7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 20
}