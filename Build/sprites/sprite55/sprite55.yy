{
    "id": "51dd8d6a-1ae9-4ec0-a713-1e0f45060d15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite55",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5ac5231-91c2-4c43-8b80-fdf9036d67b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51dd8d6a-1ae9-4ec0-a713-1e0f45060d15",
            "compositeImage": {
                "id": "2721c6e5-16eb-4637-b333-2124ca4d68b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5ac5231-91c2-4c43-8b80-fdf9036d67b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c00828a4-5382-4c06-b0da-708ef7d9a56a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5ac5231-91c2-4c43-8b80-fdf9036d67b6",
                    "LayerId": "f6990042-f4f4-4b8d-b97a-c44165acff68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "f6990042-f4f4-4b8d-b97a-c44165acff68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51dd8d6a-1ae9-4ec0-a713-1e0f45060d15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 13
}