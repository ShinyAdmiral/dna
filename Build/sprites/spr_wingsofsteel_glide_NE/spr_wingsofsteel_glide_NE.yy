{
    "id": "fc0a2e6c-163e-42b0-b0d2-b868bd0db053",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wingsofsteel_glide_NE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 42,
    "bbox_right": 53,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50f86203-a66f-445c-95aa-0d37054c5ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc0a2e6c-163e-42b0-b0d2-b868bd0db053",
            "compositeImage": {
                "id": "70653ebe-2df8-400c-94a1-a74df7b981d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f86203-a66f-445c-95aa-0d37054c5ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5a186e-0d7e-43be-9d52-08e7167caa06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f86203-a66f-445c-95aa-0d37054c5ac3",
                    "LayerId": "81ffd725-0b3d-4a82-842f-ef73e521bb50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "81ffd725-0b3d-4a82-842f-ef73e521bb50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc0a2e6c-163e-42b0-b0d2-b868bd0db053",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 54
}