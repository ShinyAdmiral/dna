{
    "id": "d3636fc3-32a9-4689-b5c5-4fbaa137b986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_default_static_S",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 40,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aea78106-8884-40a2-b73c-d1f835a54f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3636fc3-32a9-4689-b5c5-4fbaa137b986",
            "compositeImage": {
                "id": "7ca5dab1-27c5-417c-a3dc-86203607950d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea78106-8884-40a2-b73c-d1f835a54f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca579166-6018-4cdf-a4e8-3c927ee224f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea78106-8884-40a2-b73c-d1f835a54f25",
                    "LayerId": "e0abe31f-9bc0-432b-9e6f-b181fb257f5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e0abe31f-9bc0-432b-9e6f-b181fb257f5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3636fc3-32a9-4689-b5c5-4fbaa137b986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 46
}