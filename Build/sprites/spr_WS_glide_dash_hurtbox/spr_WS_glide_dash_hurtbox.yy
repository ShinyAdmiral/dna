{
    "id": "4b35ee5e-4072-4814-a148-737de9aaa6c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WS_glide_dash_hurtbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 28,
    "bbox_right": 35,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb7b39ba-f93a-42b2-a178-972caaecc6fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b35ee5e-4072-4814-a148-737de9aaa6c6",
            "compositeImage": {
                "id": "20397306-0bfa-4579-a22a-5528ccb3e04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7b39ba-f93a-42b2-a178-972caaecc6fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52002cf1-5de4-4364-b15e-e16ab2094ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7b39ba-f93a-42b2-a178-972caaecc6fa",
                    "LayerId": "4be1b65e-2948-48ac-a232-14288c616072"
                },
                {
                    "id": "20531794-8682-4346-9971-3bd156bb17a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7b39ba-f93a-42b2-a178-972caaecc6fa",
                    "LayerId": "a5d5ec31-43f9-4971-b0f4-f54d7817ecaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4be1b65e-2948-48ac-a232-14288c616072",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b35ee5e-4072-4814-a148-737de9aaa6c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a5d5ec31-43f9-4971-b0f4-f54d7817ecaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b35ee5e-4072-4814-a148-737de9aaa6c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 36
}