//Calc Movement
//calc direction
if ((x_axis != 0) || (y_axis != 0))
dir = point_direction(0,0,x_axis,y_axis);
	
//calc movement
sped = point_distance(0,0,x_axis, y_axis) * acceleration;

//calc specific axis directions
hor_speed = dcos(dir) * play_speed;
ver_speed = dsin(dir) * play_speed;

//Calc Accelleration
if (sped != 0){
	//accelerate when moveing
	play_speed += sped;
}
else{
	//decelerate when stopping
	play_speed -= (acceleration*stopping_power);
}
