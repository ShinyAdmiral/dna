#region Input Keyboard
if (!gamepad_is_connected(0)){
	//keyboad input
	move_up_player = keyboard_check(move_up);
	move_left_player = keyboard_check(move_left);
	move_right_player = keyboard_check(move_right);
	move_down_player = keyboard_check(move_down);
	roll_player = keyboard_check_pressed(roll);
	jump = keyboard_check_pressed(ord("E"));

	//calculting keyboard direction
	x_axis = move_right_player - move_left_player;
	y_axis = move_down_player - move_up_player;
	
	if (move_up_player or move_down_player or move_left_player or move_right_player){
		move_speed = original_move_speed
	}
	else{
		move_speed = 0;
	}
}
#endregion

#region Input Controller
else{
	//deadzone
	if (gamepad_get_axis_deadzone(0) != gamepad_dead_zone)
		gamepad_set_axis_deadzone(0, gamepad_dead_zone);
	
	//button inputs for gamepad
	jump = gamepad_button_check_pressed(0,gp_face2);
	
	//Gamepad input and direction calculation
	x_axis = gamepad_axis_value(0,gp_axislh);
	y_axis = gamepad_axis_value(0,gp_axislv);
	
	//calc top movespeed (analog sensitivity)
	move_speed = original_move_speed * (sqrt(sqr(x_axis) + sqr(y_axis)));
	move_speed = clamp(move_speed,0,original_move_speed);
}
#endregion