// Collisions and Movement
//clamp final calulated speed
play_speed = clamp(play_speed, 0, move_speed);

#region Horizontal
//right collision check
if (hor_speed > 0){
	//Grounded Solids---------------------------------------------------------------
	//reset instance for new instance
	insts = noone;
	
	//check for instance
	insts = (instance_place(x + abs(hor_speed),y,obj_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (insts != noone) and (insts.z > z){
		//ensures smooth and consistant collision
		while(!place_meeting(x + 1,y,insts)){
			x ++;
		}
		
		//stop
		hor_speed = 0;
	}
	
	//Floatingh Solids---------------------------------------------------------------
	//reset instance for new instance
	instf = noone;
	
	//check for instance
	instf = (instance_place(x + abs(hor_speed),y,obj_float_solid));
	
	//if instanceexists and is taller than players z stop the player
	if ((instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height)){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x + 1,y,instf))
		{
			x ++;
		}
		
		//stop
		hor_speed = 0;
	}
}

//left collision check
if (hor_speed < 0){
	//Grounded Solids---------------------------------------------------------------
	//reset instance for new instance
	insts = noone;
	
	//check for instance
	insts = (instance_place((x - abs(hor_speed)),y,obj_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (insts != noone) and (insts.z > z){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x - 1,y,insts))
		{
			x --;
		}
		
		//stop
		hor_speed = 0;
	}
	
	//Floatingh Solids---------------------------------------------------------------
	//reset instance for new instance
	instf = noone;
	
	//check for instance
	instf = (instance_place(x - abs(hor_speed),y,obj_float_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x - 1,y,instf))
		{
			x --;
		}
		
		//stop
		hor_speed = 0;
	}
}

//move player on the x axis after collision checks
x += clamp(hor_speed, -move_speed, move_speed);
#endregion

#region Vertical
//down collision check
if (ver_speed > 0){
	
	//Grounded Solids---------------------------------------------------------------
	//reset instance for new instance
	insts = noone;
	
	//check for instance
	insts = (instance_place(x,y - abs(ver_speed),obj_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (insts != noone) and (insts.z > z){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x, y - 1,insts))
		{
			y --;
		}
		
		//stop
		ver_speed = 0;
	}
	
	//Floatingh Solids---------------------------------------------------------------
	//reset instance for new instance
	instf = noone;
	
	//check for instance
	instf = (instance_place(x,y - abs(ver_speed),obj_float_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x, y - 1,instf)){
			y --;
		}
		
		//stop
		ver_speed = 0;
	}
}

// up collision check
if (ver_speed < 0){
	
	//Grounded Solids---------------------------------------------------------------
	//reset instance for new instance
	insts = noone;
	
	//check for instance
	insts = (instance_place(x,y + abs(ver_speed),obj_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (insts != noone) and (insts.z > z){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x, y + 1,insts)){
			y ++;
		}
		
		//stop
		ver_speed = 0;
	}
	
	//Floatingh Solids---------------------------------------------------------------
	//reset instance for new instance
	instf = noone;
	
	//check for instance
	instf = (instance_place(x,y + abs(ver_speed),obj_float_solid));
	
	//if instanceexists and is taller than players z stop the player
	if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
		//ensures smooth and consistant collision
		while(!place_meeting(x, y + 1,instf)){
			y ++;
		}
		
		//stop
		ver_speed = 0;
	}
}

//move player on the y axis after collision checks
y += clamp(-ver_speed, -move_speed, move_speed);
#endregion