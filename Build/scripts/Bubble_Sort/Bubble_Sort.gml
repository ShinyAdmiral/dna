array = argument[0];
length = argument[1];

var i,j;
for (i = 0; i < length - 1; i++){
	for (j = 0; j < length - 1 - i; j++){
		if (!instance_exists(array[j])){
			var temp = array[0];
			array[0] = array[j];
			array[j] = temp;
		}
		
		else if (!instance_exists(array[j+1])){
			var temp = array[0];
			array[0] = array[j+1];
			array[j+1] = temp;
		}
		
		else if (array[j].bub_id > array[j+1].bub_id){
			var temp = array[j];
			array[j] = array[j+1];
			array[j+1] = temp;
		}
	}
}

return array;