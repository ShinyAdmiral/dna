//set roll speed based on direction
roll_hor_speed = dcos(dir) * roll_speed;
roll_ver_speed = dsin(dir) * roll_speed;

//calculate roll speed 
RH_max = sqrt(sqr(roll_speed) - sqr(roll_ver_speed));
RV_max = sqrt(sqr(roll_speed) - sqr(roll_hor_speed));

//frame of anticipation. Makes the user stop before the roll
if (roll_anticipate){
	roll_anticipate = false;
	image_speed= 0;
	
	if (dir > 90 and dir < 270){
		sprite_index = spr_Roll_left;
	}
	else{
		sprite_index = spr_Roll_right;
	}
	
	image_index = 0;
	alarm[0] = room_speed * roll_anticipation_lag;
}


if (rolling){			
	if (roll_timer){
		alarm[0] = room_speed * roll_duration;
		roll_timer = false;
		image_speed = 1;
	}
			
	// Collisions and Movement
	#region Horizontal Collision
	//right collision check
	if (roll_hor_speed > 0){
		//Grounded Solids---------------------------------------------------------------
		//reset instance for new instance
		insts = noone;
	
		//check for instance
		insts = (instance_place(x + abs(roll_hor_speed),y,obj_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (insts != noone) and (insts.z > z){
			//ensures smooth and consistant collision
			while(!place_meeting(x + 1,y,insts)){
				x ++;
			}
		
			//stop
			roll_hor_speed = 0;
		}
	
		//Floatingh Solids---------------------------------------------------------------
		//reset instance for new instance
		instf = noone;
	
		//check for instance
		instf = (instance_place(x + abs(roll_hor_speed),y,obj_float_solid));
	
		//if instanceexists and is taller than players z stop the player
		if ((instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height)){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x + 1,y,instf))
			{
				x ++;
			}
		
			//stop
			roll_hor_speed = 0;
		}
	}

	//left collision check
	if (roll_hor_speed < 0){
		//Grounded Solids---------------------------------------------------------------
		//reset instance for new instance
		insts = noone;
	
		//check for instance
		insts = (instance_place(x - abs(roll_hor_speed),y,obj_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (insts != noone) and (insts.z > z){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x - 1,y,insts))
			{
				x --;
			}
		
			//stop
			roll_hor_speed = 0;
		}
	
		//Floatingh Solids---------------------------------------------------------------
		//reset instance for new instance
		instf = noone;
	
		//check for instance
		instf = (instance_place(x - abs(roll_hor_speed),y,obj_float_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x - 1,y,instf))
			{
				x --;
			}
		
			//stop
			roll_hor_speed = 0;
		}
	}

	//move player on the x axis after collision checks
	x += clamp(roll_hor_speed, -RH_max, RH_max);
	#endregion

	#region Vertical Collision
	//down collision check
	if (roll_ver_speed > 0){
	
		//Grounded Solids---------------------------------------------------------------
		//reset instance for new instance
		insts = noone;
	
		//check for instance
		insts = (instance_place(x,y - abs(roll_ver_speed),obj_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (insts != noone) and (insts.z > z){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x, y - 1,insts))
			{
				y --;
			}
		
			//stop
			roll_ver_speed = 0;
		}
	
		//Floatingh Solids---------------------------------------------------------------
		//reset instance for new instance
		instf = noone;
	
		//check for instance
		instf = (instance_place(x,y - abs(roll_ver_speed),obj_float_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x, y - 1,instf)){
				y --;
			}
		
			//stop
			roll_ver_speed = 0;
		}
	}

	// up collision check
	if (roll_ver_speed < 0){
	
		//Grounded Solids---------------------------------------------------------------
		//reset instance for new instance
		insts = noone;
	
		//check for instance
		insts = (instance_place(x,y + abs(roll_ver_speed),obj_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (insts != noone) and (insts.z > z){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x, y + 1,insts)){
				y ++;
			}
		
			//stop
			roll_ver_speed = 0;
		}
	
		//Floatingh Solids---------------------------------------------------------------
		//reset instance for new instance
		instf = noone;
	
		//check for instance
		instf = (instance_place(x,y + abs(roll_ver_speed),obj_float_solid));
	
		//if instanceexists and is taller than players z stop the player
		if (instf != noone) and (z < instf.z) and (z + height > instf.z - instf.height){
		
			//ensures smooth and consistant collision
			while(!place_meeting(x, y + 1,instf)){
				y ++;
			}
		
			//stop
			roll_ver_speed = 0;
		}
	}

	//move player on the y axis after collision checks
	y += clamp(-roll_ver_speed, -RV_max, RV_max);
	#endregion
}