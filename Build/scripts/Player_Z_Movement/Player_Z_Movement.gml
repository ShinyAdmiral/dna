//Calc Jumping
if (z <= zfloor){
	if (jump){
		zjump = true;
	}
}

if (zjump){
	inst = noone;
	inst = instance_place(x,y,obj_float_solid);
	if ((inst != noone) and (z < inst.z)){
		if (inst.z-inst.height-height > 0){
			z = clamp(z + zspeed, 0, inst.z-inst.height-height); // z pos goes up
		}
	}
	else{
		z += zspeed;
	}
}

//if not ontop of block
if ((!instance_place(x,y,obj_solid)) and (!instance_place(x,y,obj_float_solid))){
	zfloor = 0; /*zfloor is ground level*/
}

//if not on ground
if (!z <= zfloor){
	z -= zgrav; //apply downforce on z pos
	zgrav += zacce; //rav gets stronger each step
}

//stop falling when you hit zfloor
if (z <= zfloor+1/*+1 for sticking glitch on ground*/){
	zgrav = 0; //stop applying downforce
	z = zfloor;	//nap z pos to on ground
	zjump = false; //no longer in the air
}