found = false;
for (var i = 0; i < bubble_max; i++){
	if (bubble_instance[i] == noone){
		bubble_instance[i] = instance_create_depth(x,y,depth, obj_bubble);
		bubble_instance[i].bub_id = bubble_id;
		bubble_instance[i].direction = argument0;
		bubble_instance[i].speed = argument1;
		bubble_instance[i].z = z;
		bubble_id ++;
		found = true;
		break;
	}
}
				
if (!found){
	bubble_instance = Bubble_Sort(bubble_instance, bubble_max);
	instance_destroy(bubble_instance[0]);
	bubble_instance[0] = instance_create_depth(x,y,depth, obj_bubble);
	bubble_instance[0].bub_id = bubble_id;
	bubble_instance[0].direction = argument0;
	bubble_instance[0].speed = argument1;
	bubble_instance[0].z = z;
	bubble_id ++;
}