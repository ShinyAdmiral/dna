//boolean from letting the player go over the edge if they strattel there for a specified time
if (ledge_teeter){
	//make sure the player is on the object before doing the lesge functions
	if (instance_place(x,y,obj_solid) or instance_place(x,y,obj_float_solid)){
		#region Horizontal
		//Right---------------------------------------------------------------
		if (dir < 90 or dir > 270){
			if (hor_speed > 0){
				//reset ledge instance for new instance
				inst = noone;
	
				//check for ledge instance
				inst = (instance_place(x + abs(hor_speed) ,y ,obj_Teeter_Collider_Right));
	
				//if ledge instance exists and the player is on top of solid, the teeter can activate
				if (inst != noone) and (inst.z == z){
					//ensures smooth and consistant collision
					while(!place_meeting(x + 1, y, inst)){
						x ++;
					}
					
					//stop the player and active the teeter timer
					ver_speed = 0;
					hor_speed = 0;
					
					//only set the timer once (done with boolean)
					if(!teeter_right){
						alarm[1] = room_speed * teeter_time;
						teeter_right = true;
					}
				}
			}
		}
		
		//reset timer if they face the other direction of the ledge collision or move the other direction
		else if (!(dir < 90 or dir > 270) or !(hor_speed > 0)){
			teeter_right = false;
		}
		
		//Left---------------------------------------------------------------
		if (90 < dir < 270){
			if (hor_speed < 0){
				//reset ledge instance for new instance
				inst = noone;
	
				//check for ledge instance
				inst = (instance_place(x - abs(hor_speed) ,y ,obj_Teeter_Collider_Left));
	
				//if ledge instance exists and the player is on top of solid, the teeter can activate
				if (inst != noone) and (inst.z == z){
					//ensures smooth and consistant collision
					while(!place_meeting(x - 1, y, inst)){
						x --;
					}
					
					//stop the player and active the teeter timer
					ver_speed = 0;
					hor_speed = 0;
					
					//only set the timer once (done with boolean)
					if(!teeter_left){
						alarm[1] = room_speed * teeter_time;
						teeter_left = true;
					}
				}
			}
		}
		
		//reset timer if they face the other direction of the ledge collision or move the other direction
		else if (!(90 < dir < 270) or !(hor_speed < 0)){
			teeter_left = false;
		}
		#endregion
		
		//up---------------------------------------------------------------
		if (0 < dir < 180){
			if (ver_speed > 0){
				//reset ledge instance for new instance
				inst = noone;
	
				//check for instance
				inst = (instance_place(x ,y - abs(ver_speed),obj_Teeter_Collider_Up));
	
				//if ledge instance exists and the player is on top of solid, the teeter can activate
				if (inst != noone) and (inst.z == z){
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1, inst)){
						y --;
					}
					
					//stop the player and active the teeter timer
					ver_speed = 0;
					hor_speed = 0;
					
					//only set the timer once (done with boolean)
					if(!teeter_up){
						alarm[1] = room_speed * teeter_time;
						teeter_up = true;
					}
				}
			}
		}
		
		//reset timer if they face the other direction of the ledge collision or move the other direction
		else if (!(0 < dir < 180) or !(ver_speed > 0)){
			teeter_up = false;
		}
		
		//down---------------------------------------------------------------
		if (180 < dir < 360){
			if (ver_speed < 0){
				//reset ledge instance for new instance
				inst = noone;
	
				//check for instance
				inst = (instance_place(x ,y + abs(ver_speed),obj_Teeter_Collider_Down));
	
				//if ledge instance exists and the player is on top of solid, the teeter can activate
				if (inst != noone) and (inst.z == z){
					//ensures smooth and consistant collision
					while(!place_meeting(x, y - 1, inst)){
						y ++;
					}
					
					//stop the player and active the teeter timer
					ver_speed = 0;
					hor_speed = 0;
					
					//only set the timer once (done with boolean)
					if(!teeter_down){
						alarm[1] = room_speed * teeter_time;
						teeter_down = true;
					}
				}
			}
		}
		
		//reset timer if they face the other direction of the ledge collision or move the other direction
		else if (!(0 < dir < 180) or !(ver_speed < 0)){
			teeter_down = false;
		}
	}
}